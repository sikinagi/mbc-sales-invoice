package com.mbcgroup.local.salesinvoice.view;

import com.mbcgroup.local.salesinvoice.util.ADFUtils;

import java.io.IOException;

import java.io.PrintWriter;

import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCDataControl;
import oracle.adf.model.BindingContext;
import oracle.adf.model.OperationBinding;
import oracle.adf.share.ADFContext;

public class Filter2 implements Filter {
    private FilterConfig _filterConfig = null;

    public void init(FilterConfig filterConfig) throws ServletException {
        _filterConfig = filterConfig;
    }

    public void destroy() {
        _filterConfig = null;
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
                                                                                                     ServletException {
       
        HttpServletRequest req = (HttpServletRequest)request;
        HttpServletResponse res = (HttpServletResponse)response;
            HttpSession session = req.getSession(false);
                String uri = req.getRequestURI();
        
        if (session.getAttribute("SessionId") == null)
            {
                        response.setContentType("text/html");
                        PrintWriter out = response.getWriter();
                        out.println("<html>");
                        out.println("<head><title>Sales Invoice</title></head>");
                        out.println("<body>");
                        out.println("<p>Invalid Session for sales invoice</p>");
                        out.println("</body></html>");
                        out.close();
                        return;
            }

        if (uri.endsWith("login.jsf") || uri.endsWith("login")) {

                String icxSessionId = null;
                if(session.getAttribute("ICXSESSIONID") != null)
                    icxSessionId = (String)session.getAttribute("ICXSESSIONID");   
                
               if (!"".equalsIgnoreCase(icxSessionId))
               {
                    String respId = null;
                    String functionId = null;
                    String securityProfileId = null;
                    String mode = null;
                    String notificationId = null;
                    String txnHeaderId = null;
                    String orgId = null;
                    String accessLevel = null;
                    String loginId = null;
                    String employeeId = null;
                    String accessFrom = null;
                    String subRoleName = null;
                   
                    if (session.getAttribute("ResponsibilityId") != null)
                        respId = (String)session.getAttribute("ResponsibilityId");
                    else
                        respId = "-1";  
                     
//                     if (session.getAttribute("FunctionId") != null)
//                         functionId = (String)session.getAttribute("FunctionId");
                     
//                     if (session.getAttribute("Mode") != null)
//                         mode = (String)session.getAttribute("Mode");
                     
                     if (session.getAttribute("NotificationId") != null)
                         notificationId = (String)session.getAttribute("NotificationId");
                     
                     if (session.getAttribute("TxnHeaderId") != null)
                         txnHeaderId = (String)session.getAttribute("TxnHeaderId");
                     
                     if (session.getAttribute("AccessLevel") != null)
                         accessLevel = (String)session.getAttribute("AccessLevel");
                     
                     if (session.getAttribute("OrgId") != null)
                         orgId = (String)session.getAttribute("OrgId");

                     if (session.getAttribute("LoginId") != null)
                         loginId = (String)session.getAttribute("LoginId");
                     
                     if (session.getAttribute("EmployeeId") != null)
                         employeeId = (String)session.getAttribute("EmployeeId");
                     
                     if (session.getAttribute("AccessFrom") != null)
                         accessFrom = (String)session.getAttribute("AccessFrom");
                     
                     if (session.getAttribute("SubRoleName") != null)
                         subRoleName = (String)session.getAttribute("SubRoleName");

                    BindingContext bindingContext = BindingContext.getCurrent();
                    DCBindingContainer bindings = bindingContext.findBindingContainer("com_mbcgroup_local_salesinvoice_view_pageDefForFilter");
                    
                    //Invoke binding operation to validate session and session user name
                    
                    System.out.println("icxSessionId:"+icxSessionId);
                    OperationBinding sessionOper = (OperationBinding)bindings.getOperationBinding("getSessionUserName");
                    Map pSessionOperMap = sessionOper.getParamsMap();
                    pSessionOperMap.put("pSession",icxSessionId);
                    sessionOper.invoke();
                    Object ret = sessionOper.getResult();
                     
                     if (ret == null)
                       System.out.println("It does not return session name");
                     else
                     {
                        System.out.println("session User Name:"+ret.toString());
                         String sessionUserName = ret.toString();
                         if (!"N".equalsIgnoreCase(sessionUserName))
                         {
                                 ADFContext.getCurrent().getSessionScope().put("UserName", sessionUserName.toUpperCase());
                                 ADFContext.getCurrent().getSessionScope().put("RespId", respId);
//                                 ADFContext.getCurrent().getSessionScope().put("FunctionId", functionId);
                                 ADFContext.getCurrent().getSessionScope().put("ICXSESSIONID", icxSessionId);
//                                 ADFContext.getCurrent().getSessionScope().put("SecurityProfileId", securityProfileId);
//                                 ADFContext.getCurrent().getSessionScope().put("Mode", mode);
                                 ADFContext.getCurrent().getSessionScope().put("TxnHeaderId", txnHeaderId); //Testing
                                 System.out.println("from filter notification id:"+notificationId);
                                 ADFContext.getCurrent().getSessionScope().put("NotificationId", notificationId);
                                 ADFContext.getCurrent().getSessionScope().put("OrgId", orgId);
                                 ADFContext.getCurrent().getSessionScope().put("AccessLevel", accessLevel);
                                 ADFContext.getCurrent().getSessionScope().put("LoginId", loginId);
                                 ADFContext.getCurrent().getSessionScope().put("EmployeeId", employeeId);
                                 ADFContext.getCurrent().getSessionScope().put("AccessFrom", accessFrom);
                                 ADFContext.getCurrent().getSessionScope().put("SubRoleName", subRoleName);
                                 
                                 //ADFContext.getCurrent().getSessionScope().put("distPageUrl", "/page/DistPF.jsf?pDetail=true");
                                 //Invoke binding operation to set context
                                 OperationBinding contextOper = (OperationBinding)bindings.getOperationBinding("setContext");
                                 Map pContextOperMap = contextOper.getParamsMap();
                                 pContextOperMap.put("pUserName",sessionUserName);
                                 pContextOperMap.put("pSession",null);
                                 pContextOperMap.put("pResponsibilityId",respId);
                                 pContextOperMap.put("pAccessFrom",accessFrom);
                                 pContextOperMap.put("pTxnHeaderId",txnHeaderId);
                                 contextOper.invoke();

                                 String retMessage = (String)contextOper.getResult();
                                
                                 if (retMessage != null)
                                 {
                                     if (retMessage.startsWith("Error"))
                                     {
                                         response.setContentType("text/html");
                                         PrintWriter out = response.getWriter();
                                         out.println("<h2>Error information</h2>");
                                         out.println("<h2>The exception message: " + retMessage+"</h2>");                                     
                                    }
                                  }
                                 else
                                 {
                                  
                                     //Invoke binding operation to get left menu
                                     OperationBinding menuOper = (OperationBinding)bindings.getOperationBinding("getMenu");
                                     Map pMenuOperMap = menuOper.getParamsMap();
                                     pMenuOperMap.put("pRespId",Integer.parseInt(respId));
                                     pMenuOperMap.put("pMode",accessFrom);
                                     menuOper.invoke();
                                     System.out.println("a1:"+accessFrom);
                                  if (accessFrom.startsWith("WF"))  
                                  {
                                  System.out.println("refirectred to home page");
                                  res.sendRedirect(req.getContextPath() + "/faces/page/updateInvoice.jsf");
                                  }
                                     else
                                     res.sendRedirect(req.getContextPath() + "/faces/page/Home.jsf");
                                     
                                  
                                 }
                             }
                     }
                 } //if ("".equalsIgnoreCase(icxSessionId))
                } //if (uri.endsWith("login.jsf") || uri.endsWith("login"))
                else if (!uri.endsWith("login.jsf")) {
                   
                    if (session == null) 
                        res.sendRedirect( "/faces/login.jsf");
                     else {
                        //Start Logic to check if user has access to this page or not
                      
                        if (session.getAttribute("AllowedPages") != null)
                        {
                            String allowedPages = (String)session.getAttribute("AllowedPages");
                           
                            StringBuffer str = new StringBuffer(uri);
                            str = str.reverse();
                            int i = str.indexOf("/");
                            String reversePageName = str.substring(0, i);
                          
                            StringBuffer pageName = new StringBuffer(reversePageName).reverse();
                            System.out.println("pageName:"+pageName);
                            
                            if (!allowedPages.contains(pageName))
                            {
                                    response.setContentType("text/html");
                                    PrintWriter out = response.getWriter();
                                    out.println("<html>");
                                    out.println("<head><title>Sales Invoice</title></head>");
                                    out.println("<body>");
                                    out.println("<p>You are not authorized to access this function. Kindly check with System Administrator.</p>");
                                    out.println("</body></html>");
                                    out.close();
                                    return;
                             }
                        }
                        //End Logic to check if user has access to this page or not
                        
                       
                        String user = (String)session.getAttribute("UserName");
                        String sessioId = (String)session.getAttribute("ICXSESSIONID");
                      
        
                        if (user == null) 
                            res.sendRedirect(req.getContextPath() + "/faces/login.jsf");
                        else if(sessioId!=null){
                                       
                                       BindingContext bindingContext = BindingContext.getCurrent();
                                       DCBindingContainer bindings = bindingContext.findBindingContainer("com_mbcgroup_local_salesinvoice_view_pageDefForFilter");
                                       
                                       //Invoke binding operation to validate session and session user name
                                       OperationBinding sessionOper = (OperationBinding)bindings.getOperationBinding("getSessionUserName");
                                       Map pSessionOperMap = sessionOper.getParamsMap();
                                       pSessionOperMap.put("pSession",sessioId);
                                       sessionOper.invoke();
                                       Object ret = sessionOper.getResult();
                                      
                                       if ("N".equalsIgnoreCase((String)ret))
                                       {
                                               OperationBinding rollbackOper = (OperationBinding)bindings.getOperationBinding("Rollback");
                                               rollbackOper.invoke();
                                               
                                               Object logoutUrlObj = session.getAttribute("LogOutUrl");
                                               String logoutUrl = "";
                                               
                                               if (logoutUrlObj != null)
                                                   logoutUrl = (String)logoutUrlObj;
                                               
                                               res.sendRedirect(logoutUrl);
                                           }
                            
                                   }
                        
                    }
                }
            chain.doFilter(request, response);
    }
}
