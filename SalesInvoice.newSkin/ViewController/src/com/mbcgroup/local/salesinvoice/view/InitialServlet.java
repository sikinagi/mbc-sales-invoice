package com.mbcgroup.local.salesinvoice.view;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.*;
import javax.servlet.http.*;

public class InitialServlet extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=UTF-8";

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType(CONTENT_TYPE);
      
        //pRespId
            
        HttpServletRequest req = (HttpServletRequest)request;
                                HttpServletResponse res = (HttpServletResponse)response;
                                    HttpSession session = req.getSession(true);
                                        
                                        String uri = req.getRequestURI();
                                    

        String sessionId = req.getParameter("pSessionId");
        System.out.println("sessionId:"+sessionId);
       

        String icxSessionId = req.getParameter("ICXSESSIONID");
      
                                        
        String respId = req.getParameter("pRespId");
       

        String functionId = req.getParameter("pFunctionId");
        
        String subRoleName = req.getParameter("pSubRoleName");
      

//        String secProfileId = req.getParameter("pSecProfileId");
        
        
//        String mode = req.getParameter("pMode");
     
        
        String notificationId = req.getParameter("pNotificationId"); 
     
        
        String txnHeaderId = req.getParameter("pTxnHeaderId"); 
    
        
        String orgId = req.getParameter("pOrgId"); 
     

        String accessLevel = req.getParameter("pAccessLevel"); 
     
        
        String loginId = req.getParameter("pLoginId"); 
        
        String employeeId = req.getParameter("pEmployeeId");
        
        String accessFrom = req.getParameter("pAccessFrom");
        System.out.println("accessFrom 1: "+accessFrom);
   
        

          if (accessFrom == null)
            accessFrom = "APPL"; 
        
        session.setAttribute("SessionId", sessionId);
        session.setAttribute("ICXSESSIONID", icxSessionId);
        session.setAttribute("ResponsibilityId", respId);
        session.setAttribute("NotificationId", notificationId);
        session.setAttribute("TxnHeaderId", txnHeaderId);
        session.setAttribute("OrgId", orgId);
        session.setAttribute("AccessLevel", accessLevel);
        session.setAttribute("LoginId", loginId);
        session.setAttribute("EmployeeId", employeeId);
        session.setAttribute("AccessFrom", accessFrom);
        session.setAttribute("SubRoleName", subRoleName);

//        PrintWriter out = response.getWriter();
//        out.println("<html>");
//        out.println("<head><title>InitialServlet</title></head>");
//        out.println("<body>");
//        out.println("<p>The servlet has received a GET. This is the reply.</p>");
//        out.println("</body></html>");
//        out.close();
        
        res.sendRedirect(req.getContextPath() + "/faces/login.jsf");
    }
}
