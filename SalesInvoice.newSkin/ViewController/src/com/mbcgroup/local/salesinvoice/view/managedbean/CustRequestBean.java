package com.mbcgroup.local.salesinvoice.view.managedbean;

import com.mbcgroup.local.salesinvoice.util.ADFUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.SQLException;

import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.component.rich.RichPopup;

import oracle.adf.view.rich.component.rich.input.RichInputFile;

import oracle.binding.BindingContainer;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.domain.BlobDomain;

import org.apache.commons.io.IOUtils;
import org.apache.myfaces.trinidad.model.UploadedFile;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;

public class CustRequestBean {
    private RichPopup attachPopUp;
    private org.apache.myfaces.trinidad.model.UploadedFile inputFile;

    public CustRequestBean() {
    }
    
    public void setInputFile(UploadedFile inputFile) {
        this.inputFile = inputFile;
    }

    public UploadedFile getInputFile() {
        return inputFile;
    }

    public void setAttachPopUp(RichPopup attachPopUp) {
        this.attachPopUp = attachPopUp;
    }

    public RichPopup getAttachPopUp() {
        return attachPopUp;
    }

    private BlobDomain createBlobDomain(UploadedFile file) {
        InputStream inStream = null;
        OutputStream outStream = null;
        BlobDomain domain = null;

        domain = new BlobDomain();

        try {
            
            inStream = file.getInputStream();
            outStream = domain.getBinaryOutputStream();
            byte[] buffer = new byte[8192];
            int byteRead = 0;
            while ((byteRead = inStream.read(buffer, 0, 8192)) != -1) {
                outStream.write(buffer, 0, byteRead);
            }
            inStream.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return domain;
    }
    
    public String validateCustomer() {
        // Add event code here...
        FacesContext facesContext = FacesContext.getCurrentInstance();
                org.apache.myfaces.trinidad.render.ExtendedRenderKitService service = org.apache.myfaces.trinidad.util.Service.getRenderKitService(facesContext, ExtendedRenderKitService.class);
                
        DCIteratorBinding headerIterator = ADFUtils.findIterator("MaintainCustomersVO1Iterator");
        Row headerRow = headerIterator.getCurrentRow();
        
        StringBuffer fieldNames = new StringBuffer();
        String msgHeader = "Following fields are mandatory for Customer:";
       
        if (headerRow.getAttribute("OrgId") == null)
            fieldNames.append("Operating Unit");
        if (headerRow.getAttribute("CustomerName") == null)
            fieldNames.append("Customer Name");
        if (headerRow.getAttribute("TermId") == null)
            fieldNames.append("Payment Terms");
        
        if (fieldNames.length() > 0)
            return msgHeader+fieldNames.toString();
        
        return "0";
    }
    
    public void submitCustomer(ActionEvent actionEvent) {
        // Add event code here...
      
        FacesContext facesContext = FacesContext.getCurrentInstance();
        org.apache.myfaces.trinidad.render.ExtendedRenderKitService service = org.apache.myfaces.trinidad.util.Service.getRenderKitService(facesContext, ExtendedRenderKitService.class);
        StringBuffer jsScript = new StringBuffer();
        String status = "";

        status = validateCustomer();
        if (!"0".equalsIgnoreCase(status))
        {
//                jsScript.append("showMessage('','"+status+"');");
//                service.addScript(facesContext, jsScript.toString());
            
            showSweetAlertHtmlmessage("Customer", "<li><span>"+status+"</span></li>", "success");
                return;
            }
            
        OperationBinding commit = ADFUtils.findOperation("Commit");
        commit.execute();
        
        DCIteratorBinding headerIterator = ADFUtils.findIterator("MaintainCustomersVO1Iterator");
        Row headerRow = headerIterator.getCurrentRow();
      
        OperationBinding submitCustWorkflow = ADFUtils.findOperation("submitCustWorkflow");
       
        Map submitCustWorkflowParam = submitCustWorkflow.getParamsMap();
        submitCustWorkflowParam.put("pRequestId",headerRow.getAttribute("RequestId"));
        submitCustWorkflowParam.put("pUserId", ADFContext.getCurrent().getSessionScope().get("UserId"));
        submitCustWorkflowParam.put("pResponsibilityId", ADFContext.getCurrent().getSessionScope().get("RespId"));
        
        Object wfStatus = submitCustWorkflow.execute();
        
        List errors = submitCustWorkflow.getErrors();
        int errorCount = errors.size();
        System.out.println("submit wf errorCount"+errorCount);
        if (errorCount > 0)
        {
                   StringBuffer str = new StringBuffer();
                   for (int i=0; i<errorCount;i++)
                           str.append("<li><span>"+errors.get(i)+"</span></li>");

               showSweetAlertHtmlmessage("Customer", str.toString(), "error");
               return;
            }
        status = wfStatus.toString();
        System.out.println("status"+status);
        
        System.out.println("status"+status.replace("'", "."));
        
        if (!"0".equalsIgnoreCase(status))
        {
//                jsScript.append("showMessage('','"+status+"');");
//                service.addScript(facesContext, jsScript.toString());
            String statusList [] = status.split("\n");
//            System.out.println("statusList[0]:"+statusList[0]);
            StringBuffer str = new StringBuffer();
            
//            for(int i=0; i<statusList.length;i++)
//                str.append("<li><span>"+statusList[i]+"</span></li>");
                
            showSweetAlertHtmlmessage("Customer", "<li><span>"+statusList[0]+"</span></li>", "error");
                return;
            }
        
        headerRow.setAttribute("Status", "SUBMITTED");
        commit.execute();
        showSweetAlertHtmlmessage("Customer", "<li><span>customer has been submitted for approval</span></li>", "success");
    }
    
    public static void showSweetAlertHtmlmessage(String title , String text , String type)
      {
          System.out.println("inside showSweetAlertHtmlmessage");
        String className="";
         if ("success".equalsIgnoreCase(type))
         {
            className ="btn-success";
              text= "<ul class=\"msg-details\"><li><span>"+text+ "<li><span></ul>";
          }
         else if
         ("error".equalsIgnoreCase(type))
         {
              className ="btn-danger";
              text= "<ul class=\"msg-error-details\">"+text+ "</ul>";
          }
         else
         {
                 className ="btn-warning";
                 text= "<ul class=\"msg-details\"><li><span>"+text+ "<li><span></ul>";
             }
          StringBuilder alerttext=new StringBuilder();
             alerttext.append("customalertmessageHTML('");
             alerttext.append(title);
             alerttext.append("','");
             alerttext.append(text);
               alerttext.append("','");
               alerttext.append(type);
                   alerttext.append("','");
                      alerttext.append("N");
                  alerttext.append("','");
                  alerttext.append(className);
                    alerttext.append("','");
                    alerttext.append(type);
              alerttext.append("')");
            callJavaScriptMethod(alerttext.toString());     
      }
    
    public static void callJavaScriptMethod(String pAlertText)
    {
            FacesContext facesContext = FacesContext.getCurrentInstance();
                    org.apache.myfaces.trinidad.render.ExtendedRenderKitService service = org.apache.myfaces.trinidad.util.Service.getRenderKitService(facesContext, ExtendedRenderKitService.class);
                service.addScript(facesContext, pAlertText);
        }
    
    public void saveCustomer(ActionEvent actionEvent) {
        // Add event code here...
      
        FacesContext facesContext = FacesContext.getCurrentInstance();
        org.apache.myfaces.trinidad.render.ExtendedRenderKitService service = org.apache.myfaces.trinidad.util.Service.getRenderKitService(facesContext, ExtendedRenderKitService.class);
        StringBuffer jsScript = new StringBuffer();
        String status = "";

        OperationBinding commit = ADFUtils.findOperation("Commit");
        commit.execute();
        
                jsScript.append("showMessage('','"+status+"');");
                 showSweetAlertHtmlmessage("Customer", "<li><span>Saved Successfully.</span></li>", "success");
    }
    
    public void browseFile(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        
        

        FacesContext facesContext = FacesContext.getCurrentInstance();
                org.apache.myfaces.trinidad.render.ExtendedRenderKitService service = org.apache.myfaces.trinidad.util.Service.getRenderKitService(facesContext, ExtendedRenderKitService.class);
        
        if (valueChangeEvent.getNewValue() == null)
          return;
        
        UploadedFile file = (org.apache.myfaces.trinidad.model.UploadedFile)valueChangeEvent.getNewValue();  //this.getInputFile();
        
                if (file != null) {
                        
        //                        OperationBinding  createRow = ADFUtils.findOperation("CreateInsert2");
                        OperationBinding  createRow = ADFUtils.findOperation("CreateWithParams");
                        createRow.execute();
                          DCIteratorBinding attachIterator = ADFUtils.findIterator("AttachmentsVO3Iterator");
                          //RowSetIterator iterator = attachIterator.getRowSetIterator();
        
                        Row row = attachIterator.getCurrentRow();
                      
                        row.setAttribute("Description", file.getFilename());
                        row.setAttribute("FileType", file.getContentType());
                        row.setAttribute("FileContent", createBlobDomain(file));
                        row.setAttribute("EntityName", "CUSTOMER");
//                        row.setAttribute("ReferenceId", headerId);
                        //this.setInputFile(null);
                        
                        RichInputFile inputFileComponent = (RichInputFile)valueChangeEvent.getComponent();
                        inputFileComponent.resetValue();
                        inputFileComponent.setValid(false);
                        
                        
                    } else {
                             
                                RichInputFile inputFileComponent = (RichInputFile)valueChangeEvent.getComponent();
                                inputFileComponent.resetValue();
                                inputFileComponent.setValid(false);
                    
                                RichPopup popup = this.getAttachPopUp();
                                popup.hide();
                    
                                StringBuffer jsScript = new StringBuffer();
                                String errorMesg = "File is missing";
                                jsScript.append("showMessage('Attachment','"+errorMesg+"');");
                                service.addScript(facesContext, jsScript.toString());

                            }
     
    }

    public void fileDownload(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        BindingContext bindingctx = BindingContext.getCurrent();
                BindingContainer bindingcnt = bindingctx.getCurrentBindingsEntry();
                DCBindingContainer bindingImple = (DCBindingContainer) bindingcnt;
                DCIteratorBinding iterator = bindingImple.findIteratorBinding("AttachmentsVO3Iterator"); //AttachmentReadVO1Iterator

                Row row = iterator.getCurrentRow();
                BlobDomain blob = (BlobDomain) row.getAttribute("FileContent"); //getBlob(2);

            
                try {
                    IOUtils.copy(blob.getInputStream(), outputStream);
                    blob.closeInputStream();
                    outputStream.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), "");
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                }
    }
}
