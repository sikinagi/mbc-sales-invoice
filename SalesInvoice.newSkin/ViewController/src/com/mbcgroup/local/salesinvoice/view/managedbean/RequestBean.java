package com.mbcgroup.local.salesinvoice.view.managedbean;

import com.mbcgroup.local.salesinvoice.util.ADFUtils;

import java.io.IOException;
import java.io.InputStream;

import java.io.OutputStream;

import java.math.BigDecimal;

import java.sql.SQLException;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.List;

import java.util.Map;

import javax.el.ELContext;
import javax.el.ExpressionFactory;

import javax.el.ValueExpression;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import javax.servlet.http.HttpServletResponse;

import javax.servlet.http.HttpSession;

import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.LaunchPopupEvent;

import oracle.adf.view.rich.event.PopupCanceledEvent;

import oracle.adf.view.rich.event.PopupFetchEvent;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.jbo.Row;
import oracle.jbo.ViewObject;
import oracle.jbo.server.ApplicationModuleImpl;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.component.rich.input.RichInputListOfValues;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adfinternal.view.faces.model.binding.FacesCtrlLOVBinding;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.ApplicationModule;
import oracle.jbo.AttributeHints;
import oracle.jbo.RowSet;
import oracle.jbo.RowSetIterator;
import oracle.jbo.ViewCriteria;
import oracle.jbo.ViewCriteriaRow;
import oracle.jbo.domain.BlobDomain;

import org.apache.myfaces.trinidad.model.UploadedFile;

import org.apache.commons.io.IOUtils;
import javax.faces.application.Application;

import oracle.adf.view.rich.component.rich.input.RichInputFile;

import oracle.jbo.LocaleContext;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;

public class RequestBean {
    private RichTable x1;
    private org.apache.myfaces.trinidad.model.UploadedFile inputFile;
    private RichPopup createInvPopUp;
    private RichPopup attachmentPopup;
    private RichPopup kffPopup;
    private RichPopup distPopup;
    private RichPopup withdrawPopup;
    private RichPopup trackingMonthPopup;


    public RequestBean() {
    }

    public static void showSweetAlertHtmlmessage(String title , String text , String type)
      {
          
        String className="";
         if ("success".equalsIgnoreCase(type))
         {
            className ="btn-success";
              text= "<ul class=\"msg-details\"><li><span>"+text+ "<li><span></ul>";
          }
         else if
         ("error".equalsIgnoreCase(type))
         {
              className ="btn-danger";
              text= "<ul class=\"msg-error-details\">"+text+ "</ul>";
          }
         else
         {
                 className ="btn-warning";
                 text= "<ul class=\"msg-details\"><li><span>"+text+ "<li><span></ul>";
             }
          StringBuilder alerttext=new StringBuilder();
             alerttext.append("customalertmessageHTML('");
             alerttext.append(title);
             alerttext.append("','");
             alerttext.append(text);
               alerttext.append("','");
               alerttext.append(type);
                   alerttext.append("','");
                      alerttext.append("N");
                  alerttext.append("','");
                  alerttext.append(className);
                    alerttext.append("','");
                    alerttext.append(type);
              alerttext.append("')");
            callJavaScriptMethod(alerttext.toString());     
           
      }
    
    public static void callJavaScriptMethod(String pAlertText)
    {
            FacesContext facesContext = FacesContext.getCurrentInstance();
                    org.apache.myfaces.trinidad.render.ExtendedRenderKitService service = org.apache.myfaces.trinidad.util.Service.getRenderKitService(facesContext, ExtendedRenderKitService.class);
                service.addScript(facesContext, pAlertText);
        }


    public void setValueToEL(String el, Object val) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ELContext elContext = facesContext.getELContext();
        ExpressionFactory expressionFactory =   facesContext.getApplication().getExpressionFactory();
        ValueExpression exp = expressionFactory.createValueExpression(elContext, el, Object.class);
        exp.setValue(elContext, val);
    }
    
    public Object resolveExpression(String el) 
    {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ELContext elContext = facesContext.getELContext();
        ExpressionFactory expressionFactory = facesContext.getApplication().getExpressionFactory();
        ValueExpression valueExp = expressionFactory.createValueExpression(elContext, el, Object.class);
        return valueExp.getValue(elContext);
    }

    public static boolean evalAsString(String p_expression)
            {
                FacesContext context = FacesContext.getCurrentInstance();
                ExpressionFactory expressionFactory = context.getApplication().getExpressionFactory();
                ELContext elContext = context.getELContext();
                ValueExpression vex = expressionFactory.createValueExpression(elContext, p_expression, Boolean.class);
                boolean result = (Boolean) vex.getValue(elContext);
                return result;
            }


    public void clearSession(ClientEvent clientEvent) {
        // Add event code here...
       
        try{
        ExternalContext ectx = FacesContext.getCurrentInstance().getExternalContext();
            
           HttpServletResponse response = (HttpServletResponse)ectx.getResponse();
           HttpSession session = (HttpSession)ectx.getSession(false);

           
            Object logoutUrlObj = session.getAttribute("LogOutUrl");
            String logoutUrl = "";
            
            if (logoutUrlObj != null)
                logoutUrl = (String)logoutUrlObj;

           session.invalidate();
            
           response.sendRedirect(logoutUrl);
            
            FacesContext.getCurrentInstance().responseComplete();
          
        }catch(Exception e)
        {
            System.out.println("exception occurred:"+e);
            }
           return;
    }
    
    public void goToHome(ClientEvent clientEvent) {
        // Add event code here...
       
        try{
        ExternalContext ectx = FacesContext.getCurrentInstance().getExternalContext();
            
           HttpServletResponse response = (HttpServletResponse)ectx.getResponse();
           HttpSession session = (HttpSession)ectx.getSession(false);

           
            Object homeUrlObj = session.getAttribute("HomeUrl");
            String homeUrl = "";
            
            if (homeUrlObj != null)
                homeUrl = (String)homeUrlObj;

           //response.sendRedirect(ectx.getRequestContextPath()+"/faces/login.jsf");
           response.sendRedirect(homeUrl);
            
            FacesContext.getCurrentInstance().responseComplete();
           
        }catch(Exception e)
        {
            System.out.println("exception occurred:"+e);
            }
           return;
    }

    public void setInputFile(UploadedFile inputFile) {
        this.inputFile = inputFile;
    }

    public UploadedFile getInputFile() {
        return inputFile;
    }
//
    private BlobDomain createBlobDomain(UploadedFile file) {
        InputStream inStream = null;
        OutputStream outStream = null;
        BlobDomain domain = null;

        domain = new BlobDomain();

        try {
            
            inStream = file.getInputStream();
            outStream = domain.getBinaryOutputStream();
            byte[] buffer = new byte[8192];
            int byteRead = 0;
            while ((byteRead = inStream.read(buffer, 0, 8192)) != -1) {
                outStream.write(buffer, 0, byteRead);
            }
            inStream.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return domain;
    }
//    
    public void uploadFile(ActionEvent actionEvent) {
        // Add event code here...
        
        UploadedFile file = this.getInputFile();
        
                if (file != null) {
                    
                        OperationBinding  createRow = ADFUtils.findOperation("CreateInsert2");
                        createRow.execute();
                        BindingContext bindingctx = BindingContext.getCurrent();
                        BindingContainer bindingcnt = bindingctx.getCurrentBindingsEntry();
                        DCBindingContainer bindingImple = (DCBindingContainer) bindingcnt;
                        DCIteratorBinding attachIterator = bindingImple.findIteratorBinding("AttachmentsVO1Iterator");
                            
                        Row row = attachIterator.getCurrentRow();
                        row.setAttribute("Description", file.getFilename());
                        row.setAttribute("FileType", file.getContentType());
                        row.setAttribute("FileContent", createBlobDomain(file));
                        //row.setAttribute("EntityName", "SALES_INVOICE_LINE");
                        //row.setAttribute("ReferenceId", headerId);
                        
                        this.setInputFile(null);
                    } else {
                              
                                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "File is missing", "");
                                FacesContext.getCurrentInstance().addMessage(null, msg);
                            }
    }

    public void fileDownload(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        BindingContext bindingctx = BindingContext.getCurrent();
                BindingContainer bindingcnt = bindingctx.getCurrentBindingsEntry();
                DCBindingContainer bindingImple = (DCBindingContainer) bindingcnt;
                DCIteratorBinding iterator = bindingImple.findIteratorBinding("AttachmentsVO1Iterator"); //AttachmentReadVO1Iterator

                Row row = iterator.getCurrentRow();
                BlobDomain blob = (BlobDomain) row.getAttribute("FileContent"); //getBlob(2);
               
                try {
                    IOUtils.copy(blob.getInputStream(), outputStream);
                    blob.closeInputStream();
                    outputStream.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), "");
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                }
    }


    private ApplicationModuleImpl getAm() {
        FacesContext fc = FacesContext.getCurrentInstance();
        Application app = fc.getApplication();
        ExpressionFactory elFactory = app.getExpressionFactory();
        ELContext elContext = fc.getELContext();
        ValueExpression valueExp =
            elFactory.createValueExpression(elContext, "#{data.AppModuleDataControl.dataProvider}", Object.class);
        return (ApplicationModuleImpl) valueExp.getValue(elContext);
    }
    
    public void validateRequest(ActionEvent actionEvent) {
        // Add event code here...
        FacesContext facesContext = FacesContext.getCurrentInstance();
                org.apache.myfaces.trinidad.render.ExtendedRenderKitService service = org.apache.myfaces.trinidad.util.Service.getRenderKitService(facesContext, ExtendedRenderKitService.class);
                
            StringBuffer jsScript = new StringBuffer();
            jsScript.append("showMessage('Mandatory Fields','Test Msg');");
            service.addScript(facesContext, jsScript.toString());
    }

    public String validateHeader() {
        // Add event code here...
        FacesContext facesContext = FacesContext.getCurrentInstance();
                org.apache.myfaces.trinidad.render.ExtendedRenderKitService service = org.apache.myfaces.trinidad.util.Service.getRenderKitService(facesContext, ExtendedRenderKitService.class);
        StringBuffer fieldNames = new StringBuffer();                
                
        DCIteratorBinding headerIterator = ADFUtils.findIterator("TrxHeadersVO1Iterator");
        Row headerRow = headerIterator.getCurrentRow();
        
        if ( ADFContext.getCurrent().getSessionScope().get("SubRoleName") == null )
        {
                fieldNames.append("<li><span>Sub Role has not been derived.</span></li>");
                return fieldNames.toString();
            }
                  
        String subRoleName = (String)(ADFContext.getCurrent().getSessionScope().get("SubRoleName"));
        String msgHeader = "Following fields are mandatory for Header:";
       
        //Validate in all cases
        if (headerRow.getAttribute("OperatingUnitDisp") == null)
            fieldNames.append("<li><span>Operating Unit</span></li>");
        if (headerRow.getAttribute("TxnClass") == null)
            fieldNames.append("<li><span>Transaction Class</span></li>");
        if (headerRow.getAttribute("TxnTypeId") == null || headerRow.getAttribute("TransactionTypeDisp") == null)
            fieldNames.append("<li><span>Transaction Type</span></li>");
        if (headerRow.getAttribute("Currency") == null)
            fieldNames.append("<li><span>Currency</span></li>");
        if (headerRow.getAttribute("CustAccountId") == null || headerRow.getAttribute("CustomerNameDisp") == null) 
            fieldNames.append("<li><span>Customer Name</span></li>");
        if (headerRow.getAttribute("SiteUseId") == null || headerRow.getAttribute("LocationDisp") == null || headerRow.getAttribute("LocationId") == null)
            fieldNames.append("<li><span>Bill To Location</span></li>");
        if (headerRow.getAttribute("ContactId") == null || headerRow.getAttribute("ContactNameDisp") == null)
            fieldNames.append("<li><span>Contact Name</span></li>");
        
        

        //Validate only in case of Review
        if ("REVIEW".equalsIgnoreCase(subRoleName) && (headerRow.getAttribute("TermId") == null || headerRow.getAttribute("TermNameDisp") == null))
            fieldNames.append("<li><span>Payment Terms</span></li>");
        if ("REVIEW".equalsIgnoreCase(subRoleName) && headerRow.getAttribute("GlDate") == null)
            fieldNames.append("<li><span>GL Date</span></li>");
        if ("REVIEW".equalsIgnoreCase(subRoleName) && (headerRow.getAttribute("BankAccountId") == null || headerRow.getAttribute("BankAccountNameDisp") == null))
            fieldNames.append("<li><span>Bank Account</span></li>");
        

            if (fieldNames.length() > 0)
                return msgHeader+fieldNames.toString();
            
            return "0";
    }

    public String validateLines() {
        // Add event code here...
        FacesContext facesContext = FacesContext.getCurrentInstance();
                org.apache.myfaces.trinidad.render.ExtendedRenderKitService service = org.apache.myfaces.trinidad.util.Service.getRenderKitService(facesContext, ExtendedRenderKitService.class);
        StringBuffer fieldNames = new StringBuffer();
        StringBuffer jsScript = new StringBuffer();
        fieldNames.append("Following fields are mandatory for Lines:");
        fieldNames.append("");
        boolean fieldMissing = false;
        
        DCIteratorBinding lineIterator = ADFUtils.findIterator("TrxLinesVO1Iterator");
        RowSetIterator rsi = lineIterator.getRowSetIterator();
        
        int lineCount = rsi.getRowCount();
        Row lineRow = null;
        for (int i = 0; i<lineCount; i++)
        {
               lineRow = rsi.getRowAtRangeIndex(i);
              if (lineRow != null)
              {
                      if (lineRow.getAttribute("UnitPrice") == null)
                      {
                          fieldMissing = true;
                          fieldNames.append("<li><span>Unit Price</span></li>");
                      }
                      if (lineRow.getAttribute("Quantity") == null)
                      {
                          fieldMissing = true;
                          fieldNames.append("<li><span>Quantity</span></li>");
                      }
                      if (lineRow.getAttribute("TaxRateCode") == null)
                      {
                          fieldMissing = true;
                          fieldNames.append("<li><span>Tax Code</span></li>");
                      }
                      if (lineRow.getAttribute("TaxRate") == null)
                      {
                          fieldMissing = true;
                          fieldNames.append("<li><span>Tax Rate</span></li>");
                      }
                      if (lineRow.getAttribute("ServiceDate") == null)
                      {
                          fieldMissing = true;
                          fieldNames.append("<li><span>Service Date</span></li>");
                      }
              }
            
                if (fieldMissing)
                        break;
            }
        
                if (fieldMissing)
                  return  fieldNames.toString();                
                else
                  return "0";
    }
    
    public List<SelectItem> suggestOperatingUnit(String pOuName) {
        // Add event code here...
        
        List<SelectItem> documentList = new ArrayList<SelectItem>();
                if (pOuName != null) {
                    BindingContext bindingctx = BindingContext.getCurrent();
                    BindingContainer bindingcnt = bindingctx.getCurrentBindingsEntry();
                    DCIteratorBinding OuNameIter = (DCIteratorBinding) bindingcnt.get("OperatingUnitsVO1Iterator");
                    ViewObject ouNameVO = OuNameIter.getViewObject();

                    //ouNameVO.clearCache();
                    //vendorNameVO.setWhereClause("VENDOR_NAME like upper('"+pVendorName+"%')");
                    //vendorNameVO.executeQuery();
                    
                    System.out.println("Row count:"+ouNameVO.getRowCount());
                    int count = 0;
                    while (ouNameVO.hasNext()) {
                        Row row = ouNameVO.next();
                      
                        String matchingEntries = (String) row.getAttribute("OperatingUnitName");
                        if (matchingEntries.toUpperCase().startsWith(pOuName.toUpperCase())) {
                        //if (pVendorName.toUpperCase().contains(matchingEntries.toUpperCase())) {
                            documentList.add(new SelectItem(matchingEntries, matchingEntries));
                          
                            count = count + 1;
//                            if (count > 1)
//                            break;
                        }
                    }
                }
        return documentList;
    }

    public void launchOperatingUnit(LaunchPopupEvent launchPopupEvent) {
        // Add event code here...
        String documentValue = (String) launchPopupEvent.getSubmittedValue();
                
                if (documentValue!=null &&documentValue.length()>0){
                        RichInputListOfValues lovComp = (RichInputListOfValues)launchPopupEvent.getComponent();
                        FacesCtrlLOVBinding.ListOfValuesModelImpl lovModel = null;
                        lovModel = (FacesCtrlLOVBinding.ListOfValuesModelImpl)lovComp.getModel();
                        }
    }

    public void cancelInvoice(ActionEvent actionEvent) {
        // Add event code here...
        DCIteratorBinding headerIterator = ADFUtils.findIterator("TrxHeadersVO1Iterator");
        Row headerRow = headerIterator.getCurrentRow();
        OperationBinding rollback = ADFUtils.findOperation("Rollback");
        rollback.execute();
    }

    public void splitLines(ActionEvent actionEvent) {
        // Add event code here...
        
        StringBuffer jsScript = new StringBuffer();
        FacesContext facesContext = FacesContext.getCurrentInstance();
                org.apache.myfaces.trinidad.render.ExtendedRenderKitService service = org.apache.myfaces.trinidad.util.Service.getRenderKitService(facesContext, ExtendedRenderKitService.class);

        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        
        if (ADFContext.getCurrent().getPageFlowScope().get("action") == null)
            showSweetAlertHtmlmessage("Sales Invoice", "<li><span>Unable to show distributions. Kindly check action.</span></li>", "error");
        
        String action = (String)(ADFContext.getCurrent().getPageFlowScope().get("action"));
        
        if ("QUERY".equalsIgnoreCase(action))
        {
            this.getDistPopup().show(hints);
            return;
        }
        
                DCIteratorBinding distIter = ADFUtils.findIterator("DistributionsEO1Iterator");
                //RowSetIterator iterator = distIter.getRowSetIterator();
                OperationBinding createInsert = ADFUtils.findOperation("CreateInsert1");
                
            if (distIter.getCurrentRow() != null)
            {
                this.getDistPopup().show(hints);
                return;
            }
       
        DCIteratorBinding lineIter = ADFUtils.findIterator("TrxLinesVO1Iterator");       
        Row lineRow = lineIter.getCurrentRow();
        
        
        if (   (lineRow.getAttribute("DistCount") == null) 
    //            || (lineRow.getAttribute("Amount") == null)
            || (lineRow.getAttribute("TaxRateCode") == null)
               || (lineRow.getAttribute("Quantity") == null)
                || (lineRow.getAttribute("UnitPrice") == null )
            || (lineRow.getAttribute("AmountPercent") == null )
           )
        {
                StringBuffer msg = new StringBuffer();
                msg.append("Kindly enter followings:");
                msg.append("Distribution Count");
                msg.append("Quantity");
                msg.append("Unit Price");
                msg.append("Tax Rate Code");
                msg.append("Distribution Criteria");
                jsScript.append("showMessage('Mandatory','"+msg.toString()+"');");
                service.addScript(facesContext, jsScript.toString());
                return;
            }
        else if (lineRow.getAttribute("TaxRate") == null)
        {
                StringBuffer msg = new StringBuffer();
                msg.append("Tax Rate is missing for Tax Rate Code");
                service.addScript(facesContext, jsScript.toString());
                return;
            }
        
        oracle.jbo.domain.Number distCountNumber = (oracle.jbo.domain.Number)lineRow.getAttribute("DistCount");
        oracle.jbo.domain.Number amountNumber = (oracle.jbo.domain.Number)(((oracle.jbo.domain.Number)lineRow.getAttribute("Quantity")).multiply((oracle.jbo.domain.Number)lineRow.getAttribute("UnitPrice"))).round(2);
        lineRow.setAttribute("AmountDisp", amountNumber);
            //(oracle.jbo.domain.Number)lineRow.getAttribute("Amount");
        
        if (distCountNumber.compareTo(0) <= 0)
            {
                    StringBuffer msg = new StringBuffer();
                    msg.append("Distribution count must be positive");
                    jsScript.append("showMessage('','"+msg+"');");
                    service.addScript(facesContext, jsScript.toString());
                    return;
                }
            
            int distCount = distCountNumber.intValue();
        
            oracle.jbo.domain.Number distTotal = new oracle.jbo.domain.Number(0);
            oracle.jbo.domain.Number amtTotal = new oracle.jbo.domain.Number(0);
            
            //Logic to generate distributions
            for (int j = 0; j < distCount; j++) {
                oracle.jbo.domain.Number amount = amountNumber.divide(distCountNumber);
                oracle.jbo.domain.Number percent = new oracle.jbo.domain.Number(100).divide(distCountNumber);
                
                createInsert.execute();
                
                Row r = distIter.getCurrentRow(); // itertor.createRow();
                
                oracle.jbo.domain.Number percentTemp = new oracle.jbo.domain.Number(percent.round(0));
                oracle.jbo.domain.Number amountTemp = new oracle.jbo.domain.Number(amountNumber.multiply(percentTemp.divide(100)).round(0));
                
                if (j == distCount - 1) {
                                            percentTemp = new oracle.jbo.domain.Number(100).subtract(distTotal);
                                            amountTemp = amountNumber.subtract(amtTotal);
                                        }
               
                
                if (j == (distCount-1))
                {
                    amtTotal = amtTotal.add(amountTemp.floatValue());
                    r.setAttribute("Amount", amountTemp);
                    //amtTotal = amtTotal.add(amountTemp.floatValue());
                }
                else
                {
                    r.setAttribute("Amount", amountTemp.round(0));
                    amtTotal = amtTotal.add(amountTemp.round(0).floatValue());
                }
                
                r.setAttribute("Percentage", percentTemp.round(0));
//                iterator.insertRow(r);
//                iterator.setCurrentRow(r);
                
                distTotal = distTotal.add(percentTemp.round(0).floatValue());
                //amtTotal = amtTotal.add(amountTemp.round(0).floatValue());
            }
            
            //Logic to default Tax Line
            oracle.jbo.domain.Number taxRateNumber = (oracle.jbo.domain.Number)lineRow.getAttribute("TaxRate");
            oracle.jbo.domain.Number temp = new oracle.jbo.domain.Number(100);
            oracle.jbo.domain.Number taxAmount = new oracle.jbo.domain.Number(taxRateNumber.multiply(amountNumber.divide(temp)).round(2));
          
            createInsert.execute();
            Row taxRow=null;
             taxRow = distIter.getCurrentRow(); // iterator.createRow();
            taxRow.setAttribute("Amount", taxAmount); //taxAmount
            taxRow.setAttribute("Percentage", taxRateNumber);  //taxRateNumber
            taxRow.setAttribute("ChannelCode", "000");
            taxRow.setAttribute("LineType", "TAX");
            
        distIter.executeQuery();
        
//        RichPopup.PopupHints hints = new RichPopup.PopupHints();
                this.getDistPopup().show(hints);
    }

    public void createLine(ActionEvent actionEvent) {
        // Add event code here...

        FacesContext facesContext = FacesContext.getCurrentInstance();
                org.apache.myfaces.trinidad.render.ExtendedRenderKitService service = org.apache.myfaces.trinidad.util.Service.getRenderKitService(facesContext, ExtendedRenderKitService.class);
        StringBuffer jsScript = new StringBuffer();
        
        DCIteratorBinding headerIter = ADFUtils.findIterator("TrxHeadersVO1Iterator");
        Row headerRow = headerIter.getCurrentRow();
        
        if (headerRow == null)
        {
                jsScript.append("showMessage('Mandatory Fields','Kindly enter header details');");
                service.addScript(facesContext, jsScript.toString());
                return;
            }
        else if (headerRow.getAttribute("CountryDisp") == null)
        {
                jsScript.append("showMessage('Mandatory Fields','Kindly enter customer location');");
                service.addScript(facesContext, jsScript.toString());
                return;
            }
        else if (headerRow.getAttribute("OrgId") == null)
        {
                jsScript.append("showMessage('Mandatory Fields','Kindly enter Operating Unit');");
                service.addScript(facesContext, jsScript.toString());
                return;
            }
        
        boolean withCountryFlag = false;
        OperationBinding createInsert = ADFUtils.findOperation("CreateInsert");
        createInsert.execute();

        String country = (String)headerRow.getAttribute("CountryDisp");   
        country = country.toUpperCase();
        
        DCIteratorBinding lineIter = ADFUtils.findIterator("TrxLinesVO1Iterator"); 
        Row lineRow = lineIter.getCurrentRow();
        lineRow.setAttribute("OrgId", headerRow.getAttribute("OrgId"));

        if (country != null)
        {
                DCIteratorBinding taxIter = ADFUtils.findIterator("TaxCodeVO1Iterator");
                ViewObject taxVO = taxIter.getViewObject();
                //taxVO.setWhereClause(null);
                taxVO.setWhereClauseParams(null);
                taxVO.setNamedWhereClauseParam("pOrgId", headerRow.getAttribute("OrgId"));
                if (
                      country.contains("ARAB") 
                   || country.contains("UAE")
                   || country.contains("U.A.E.")
                   || country.contains("AE")
                   )
                   {
                        
                        taxVO.setWhereClause("upper(tax_rate_code) = 'UAE VAT STD RATE'");
                        withCountryFlag = true;
                   }
                
                if (!withCountryFlag)
                    taxVO.setWhereClause("upper(tax_rate_code) = 'UAE VAT ZERO RATE'");
             
                taxVO.executeQuery();
                if (taxVO.first() != null)
                {

                    lineRow.setAttribute("TaxRateCode", taxVO.first().getAttribute("TaxRateCode"));
                    lineRow.setAttribute("TaxId", taxVO.first().getAttribute("TaxId"));
                    lineRow.setAttribute("TaxRate", taxVO.first().getAttribute("TaxRate"));
                    lineRow.setAttribute("TaxJurisdictionCode", taxVO.first().getAttribute("TaxJurisdictionCode"));
                    lineRow.setAttribute("TaxStatus", taxVO.first().getAttribute("TaxStatusCode"));
                    lineRow.setAttribute("Tax", taxVO.first().getAttribute("Tax"));
                    lineRow.setAttribute("TaxRegimeCode", taxVO.first().getAttribute("TaxRegimeCode"));
           
                }
            }
        
    }

    public void currencyChange(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
 
        if (valueChangeEvent.getNewValue() != null)
        {

            String currency = (String)valueChangeEvent.getNewValue();
            DCIteratorBinding headerIter = ADFUtils.findIterator("TrxHeadersVO1Iterator"); 
            Row headerRow = headerIter.getCurrentRow();
            
            if (currency.equalsIgnoreCase("AED"))
            {
                    headerRow.setAttribute("ExchangeRateType", "Corporate");
                    headerRow.setAttribute("ExchangeRate", null);
            }
                    
        }
    }

    public List<SelectItem> suggestCustomers(String pCustomerName) {
        // Add event code here...
      
        List<SelectItem> documentList = new ArrayList<SelectItem>();
                if (pCustomerName != null) {
                    BindingContext bindingctx = BindingContext.getCurrent();
                    BindingContainer bindingcnt = bindingctx.getCurrentBindingsEntry();
                    DCIteratorBinding customerIter = (DCIteratorBinding) bindingcnt.get("CustomersVO1Iterator");
                    
                    DCIteratorBinding headerIter = (DCIteratorBinding) bindingcnt.get("TrxHeadersVO1Iterator");
                    Row headerRow = headerIter.getCurrentRow();
                    ViewObject customerVO = customerIter.getViewObject();

                    //customerVO.clearCache();
                    customerVO.setNamedWhereClauseParam("pOrgId", headerRow.getAttribute("OrgId"));
                    customerVO.setWhereClause("upper(party_name) like upper('"+pCustomerName+"%')");
                    customerVO.executeQuery();
                    
                   
                    int count = 0;
                    while (customerVO.hasNext()) {
                        Row row = customerVO.next();
                       
                        String matchingEntries = (String) row.getAttribute("PartyName");
                        if (matchingEntries.toUpperCase().startsWith(pCustomerName.toUpperCase())) {
                        //if (pVendorName.toUpperCase().contains(matchingEntries.toUpperCase())) {
                            documentList.add(new SelectItem(matchingEntries, matchingEntries));
                            
                            count = count + 1;
                                    if (count > 4)
                                    break;
                        }
                    }
                }
        return documentList;
    }

    public void launcgCustomerLov(LaunchPopupEvent launchPopupEvent) {
        // Add event code here...
        String documentValue = (String) launchPopupEvent.getSubmittedValue();
                
                if (documentValue!=null &&documentValue.length()>0){
                        RichInputListOfValues lovComp = (RichInputListOfValues)launchPopupEvent.getComponent();
                        FacesCtrlLOVBinding.ListOfValuesModelImpl lovModel = null;
                        lovModel = (FacesCtrlLOVBinding.ListOfValuesModelImpl)lovComp.getModel();
                            
                        
                        }
    }

    public List<SelectItem> suggestCustomerLocation(String pLocation) {
        // Add event code here...
       
        
        List<SelectItem> documentList = new ArrayList<SelectItem>();
                if (pLocation != null) {
                    BindingContext bindingctx = BindingContext.getCurrent();
                    BindingContainer bindingcnt = bindingctx.getCurrentBindingsEntry();
                    DCIteratorBinding locationIter = (DCIteratorBinding) bindingcnt.get("CustomerBillToVO1Iterator");
                    
                    DCIteratorBinding headerIter = (DCIteratorBinding) bindingcnt.get("TrxHeadersVO1Iterator");
                    Row headerRow = headerIter.getCurrentRow();
                    ViewObject locationVO = locationIter.getViewObject();

                    //customerVO.clearCache();
                    locationVO.setNamedWhereClauseParam("pOrgId", headerRow.getAttribute("OrgId"));
                    locationVO.setNamedWhereClauseParam("pCustAccountId", headerRow.getAttribute("CustAccountId"));
                    locationVO.setWhereClause("upper(site_location) like upper('"+pLocation+"%')");
                    locationVO.executeQuery();
                    
                   
                    int count = 0;
                    while (locationVO.hasNext()) {
                        Row row = locationVO.next();
                       
                        String matchingEntries = (String) row.getAttribute("SiteLocation");
                        if (matchingEntries.toUpperCase().startsWith(pLocation.toUpperCase())) {
                        //if (pVendorName.toUpperCase().contains(matchingEntries.toUpperCase())) {
                            documentList.add(new SelectItem(matchingEntries, matchingEntries));
                          
                            count = count + 1;
                                    if (count > 4)
                                    break;
                        }
                    }
                }
        return documentList;
    }

    public void launchLocationLov(LaunchPopupEvent launchPopupEvent) {
        // Add event code here...
        String documentValue = (String) launchPopupEvent.getSubmittedValue();
              
                if (documentValue!=null &&documentValue.length()>0){
                        RichInputListOfValues lovComp = (RichInputListOfValues)launchPopupEvent.getComponent();
                        FacesCtrlLOVBinding.ListOfValuesModelImpl lovModel = null;
                        lovModel = (FacesCtrlLOVBinding.ListOfValuesModelImpl)lovComp.getModel();
                        }
    }

    public void launchContactsLov(LaunchPopupEvent launchPopupEvent) {
        // Add event code here...
        String documentValue = (String) launchPopupEvent.getSubmittedValue();
             
                if (documentValue!=null &&documentValue.length()>0){
                        RichInputListOfValues lovComp = (RichInputListOfValues)launchPopupEvent.getComponent();
                        FacesCtrlLOVBinding.ListOfValuesModelImpl lovModel = null;
                        lovModel = (FacesCtrlLOVBinding.ListOfValuesModelImpl)lovComp.getModel();
                      
                        }
    }

    public List<SelectItem> suggestContacts(String pContact) {
        // Add event code here...
        //CustSiteContactsVO1Iterator
    
        
        List<SelectItem> documentList = new ArrayList<SelectItem>();
                if (pContact != null) {
                    BindingContext bindingctx = BindingContext.getCurrent();
                    BindingContainer bindingcnt = bindingctx.getCurrentBindingsEntry();
                    DCIteratorBinding contactIter = (DCIteratorBinding) bindingcnt.get("CustSiteContactsVO1Iterator");
                    
                    DCIteratorBinding headerIter = (DCIteratorBinding) bindingcnt.get("TrxHeadersVO1Iterator");
                    Row headerRow = headerIter.getCurrentRow();
                    ViewObject contactVO = contactIter.getViewObject();
                    
                    //customerVO.clearCache();
                    contactVO.setNamedWhereClauseParam("pAddressId", headerRow.getAttribute("AddressId"));
                    contactVO.setWhereClause("upper(party_name) like upper('"+pContact+"%')");
                    contactVO.executeQuery();
                    
                    int count = 0;
                    while (contactVO.hasNext()) {
                        Row row = contactVO.next();
                       
                        String matchingEntries = (String) row.getAttribute("PartyName");
                        if (matchingEntries.toUpperCase().startsWith(pContact.toUpperCase())) {
                        //if (pVendorName.toUpperCase().contains(matchingEntries.toUpperCase())) {
                            documentList.add(new SelectItem(matchingEntries, matchingEntries));
                            
                            count = count + 1;
                                    if (count > 4)
                                    break;
                        }
                    }
                }
        return documentList;
    }

    public List<SelectItem> suggestCurrency(String pCurrency) {
        // Add event code here...
        
        List<SelectItem> documentList = new ArrayList<SelectItem>();
                if (pCurrency != null) {
                    BindingContext bindingctx = BindingContext.getCurrent();
                    BindingContainer bindingcnt = bindingctx.getCurrentBindingsEntry();
                    DCIteratorBinding contactIter = (DCIteratorBinding) bindingcnt.get("CurrencyVO1Iterator");
                    
                    DCIteratorBinding headerIter = (DCIteratorBinding) bindingcnt.get("TrxHeadersVO1Iterator");
                    Row headerRow = headerIter.getCurrentRow();
                    ViewObject contactVO = contactIter.getViewObject();
                    
                    //customerVO.clearCache();
                    //contactVO.setNamedWhereClauseParam("pAddressId", headerRow.getAttribute("Currency"));
                    contactVO.setWhereClause("upper(currency_code) like upper('"+pCurrency+"%')");
                    contactVO.executeQuery();
                    
                    
                    int count = 0;
                    while (contactVO.hasNext()) {
                        Row row = contactVO.next();
                       
                        String matchingEntries = (String) row.getAttribute("CurrencyCode");
                        if (matchingEntries.toUpperCase().startsWith(pCurrency.toUpperCase())) {
                        //if (pVendorName.toUpperCase().contains(matchingEntries.toUpperCase())) {
                            documentList.add(new SelectItem(matchingEntries, matchingEntries));
                           
                            count = count + 1;
                                    if (count > 4)
                                    break;
                        }
                    }
                }
        return documentList;
    }
    
    public void launchCurrencyLov(LaunchPopupEvent launchPopupEvent) {
        // Add event code here...
        String documentValue = (String) launchPopupEvent.getSubmittedValue();
             
                if (documentValue!=null &&documentValue.length()>0){
                        RichInputListOfValues lovComp = (RichInputListOfValues)launchPopupEvent.getComponent();
                        FacesCtrlLOVBinding.ListOfValuesModelImpl lovModel = null;
                        lovModel = (FacesCtrlLOVBinding.ListOfValuesModelImpl)lovComp.getModel();
                            
                       
                        }
    }
    
    public List<SelectItem> suggestArTerm(String pPayTerm) {
        // Add event code here...
        //CustSiteContactsVO1Iterator
      
        
        List<SelectItem> documentList = new ArrayList<SelectItem>();
                if (pPayTerm != null) {
                    BindingContext bindingctx = BindingContext.getCurrent();
                    BindingContainer bindingcnt = bindingctx.getCurrentBindingsEntry();
                    DCIteratorBinding contactIter = (DCIteratorBinding) bindingcnt.get("ArTermsVO1Iterator");
                    
                    DCIteratorBinding headerIter = (DCIteratorBinding) bindingcnt.get("TrxHeadersVO1Iterator");
                    Row headerRow = headerIter.getCurrentRow();
                    ViewObject contactVO = contactIter.getViewObject();
                    
                    //customerVO.clearCache();
                    //contactVO.setNamedWhereClauseParam("pAddressId", headerRow.getAttribute("Currency"));
                    contactVO.setWhereClause("upper(term_name) like upper('"+pPayTerm+"%')");
                    contactVO.executeQuery();
                    
                   
                    int count = 0;
                    while (contactVO.hasNext()) {
                        Row row = contactVO.next();
                       
                        String matchingEntries = (String) row.getAttribute("TermName");
                        if (matchingEntries.toUpperCase().startsWith(pPayTerm.toUpperCase())) {
                        //if (pVendorName.toUpperCase().contains(matchingEntries.toUpperCase())) {
                            documentList.add(new SelectItem(matchingEntries, matchingEntries));
                           
                            count = count + 1;
                                    if (count > 4)
                                    break;
                        }
                    }
                }
        return documentList;
    }
    
    public void launchPayTermLov(LaunchPopupEvent launchPopupEvent) {
        // Add event code here...
        String documentValue = (String) launchPopupEvent.getSubmittedValue();
            
                if (documentValue!=null &&documentValue.length()>0){
                        RichInputListOfValues lovComp = (RichInputListOfValues)launchPopupEvent.getComponent();
                        FacesCtrlLOVBinding.ListOfValuesModelImpl lovModel = null;
                        lovModel = (FacesCtrlLOVBinding.ListOfValuesModelImpl)lovComp.getModel();
                            
                     
                        }
    }
    
    public void browseFile(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
      

        FacesContext facesContext = FacesContext.getCurrentInstance();
                org.apache.myfaces.trinidad.render.ExtendedRenderKitService service = org.apache.myfaces.trinidad.util.Service.getRenderKitService(facesContext, ExtendedRenderKitService.class);

        if (valueChangeEvent.getNewValue() == null)
          return;
        
        UploadedFile file = (org.apache.myfaces.trinidad.model.UploadedFile)valueChangeEvent.getNewValue();  //this.getInputFile();
       
        
                if (file != null) {
                    
//                        OperationBinding  createRow = ADFUtils.findOperation("CreateInsert2");
                        OperationBinding  createRow = ADFUtils.findOperation("CreateWithParams");
                        createRow.execute();
                          DCIteratorBinding attachIterator = ADFUtils.findIterator("AttachmentsVO1Iterator");
                       
                        Row row = attachIterator.getCurrentRow();
                        
                       
                        row.setAttribute("Description", file.getFilename());
                        row.setAttribute("FileType", file.getContentType());
                        row.setAttribute("FileContent", createBlobDomain(file));
                   
                        
                        RichInputFile inputFileComponent = (RichInputFile)valueChangeEvent.getComponent();
                        inputFileComponent.resetValue();
                        inputFileComponent.setValid(false);
                        
                        
                    } else {
                                
                                RichInputFile inputFileComponent = (RichInputFile)valueChangeEvent.getComponent();
                                inputFileComponent.resetValue();
                                inputFileComponent.setValid(false);
                    
                              
                                RichPopup popup = this.getAttachmentPopup();
                                popup.hide();
                    
                                StringBuffer jsScript = new StringBuffer();
                    String errorMesg = "File is missing";
                                                    jsScript.append("showMessage('Attachment','"+errorMesg+"');");
                                service.addScript(facesContext, jsScript.toString());
                                return;      
                            }
        
    }

    public void setAttachmentPopup(RichPopup attachmentPopup) {
        this.attachmentPopup = attachmentPopup;
    }

    public RichPopup getAttachmentPopup() {
        return attachmentPopup;
    }

    public void openKff(ActionEvent actionEvent) {
        // Add event code here...
       
        FacesContext facesContext = FacesContext.getCurrentInstance();
        org.apache.myfaces.trinidad.render.ExtendedRenderKitService service = org.apache.myfaces.trinidad.util.Service.getRenderKitService(facesContext, ExtendedRenderKitService.class);
        StringBuffer jsScript = new StringBuffer();
                    
                    DCIteratorBinding iterator = ADFUtils.findIterator("TrxHeadersVO1Iterator");
                    Row r = iterator.getCurrentRow();
                   
                    
                    if (r.getAttribute("OrgId") == null)
                    {
                            ADFContext.getCurrent().getSessionScope().put("OrgId", null);
                            
//                            RichPopup kffPopup = this.getKffPopup();
//                            kffPopup.hide();
                                                     
                            jsScript.append("showMessage('Mandatory Fields','Enter Operating Unit');");
                            service.addScript(facesContext, jsScript.toString());
                            return;

                        }
                    ADFContext.getCurrent().getSessionScope().put("OrgId", r.getAttribute("OrgId"));
        
        DCIteratorBinding kffIter = ADFUtils.findIterator("KffMetaDataVO1Iterator");
      
        ViewObject metaDataVO = kffIter.getViewObject();
        metaDataVO.setNamedWhereClauseParam("pOrgId", r.getAttribute("OrgId"));
        metaDataVO.executeQuery();
            
        RowSetIterator rsi = kffIter.getRowSetIterator();
        int rowCount = rsi.getRowCount();
        //int rowCount = metaDataVO.getRowCount();
       
        if (rowCount <= 0)
        {
                jsScript.append("showMessage('Kindly check Accounting flexfield associated with this OU','');");
                service.addScript(facesContext, jsScript.toString());
                return;
            }
        
        oracle.jbo.domain.Number tempIdFlexNum = new oracle.jbo.domain.Number();
        for(int i = 0; i< rowCount; i++)
        {
             Row kffRow = rsi.getRowAtRangeIndex(i);
             //Row kffRow = metaDataVO.getRowAtRangeIndex(i);
            
              if (kffRow != null)
              {
                      
                      if (i == 0)
                      {
                          ADFContext.getCurrent().getSessionScope().put("ChartOfAccountsId", kffRow.getAttribute("ChartOfAccountsId"));
                          ADFContext.getCurrent().getSessionScope().put("ApplicationShortName", kffRow.getAttribute("ApplicationShortName"));
                          ADFContext.getCurrent().getSessionScope().put("IdFlexCode", kffRow.getAttribute("IdFlexCode"));
                      }
                      
                      //check if multiple accounting flexfield structure associated with this OU
                      if (i> 0)
                      {
                          if ( tempIdFlexNum.compareTo((oracle.jbo.domain.Number)kffRow.getAttribute("IdFlexNum")) != 0 )
                          {
                                  jsScript.append("showMessage('Multiple Accounting flexfield associated','');");
                                  service.addScript(facesContext, jsScript.toString());
                                  return;
                           }
                      }
                     ADFContext.getCurrent().getSessionScope().put("segment"+(i+1)+"Prompt", kffRow.getAttribute("FormLeftPrompt"));
                     ADFContext.getCurrent().getSessionScope().put("segment"+(i+1)+"ValueSet", kffRow.getAttribute("FlexValueSetId"));
                      
                      tempIdFlexNum = (oracle.jbo.domain.Number)kffRow.getAttribute("IdFlexNum");
                  }
            
            }
        
        //logic to clear previously holded code combination in KFF popup
        DCIteratorBinding holdKffIter = ADFUtils.findIterator("HoldKffVO1Iterator");
        ViewObject vo = holdKffIter.getViewObject();
        vo.executeQuery();
        
        //Query GLCodeCombinationsVO1 for existing distribution while opening popup
        vo.first().setAttribute("Segment1", "150");
        vo.first().setAttribute("Segment2", "0000");
        vo.first().setAttribute("Segment3", "140905");

        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        this.getKffPopup().show(hints);

    }

    public void setKffPopup(RichPopup kffPopup) {
        this.kffPopup = kffPopup;
    }

    public RichPopup getKffPopup() {
        return kffPopup;
    }

   public void queryGLCode()
   {
           DCIteratorBinding holdKffIter = ADFUtils.findIterator("HoldKffVO1Iterator");
           Row holdKffRow = holdKffIter.getCurrentRow();
               
           DCIteratorBinding glIter = ADFUtils.findIterator("GLCodeCombinationsVO1Iterator");
           ViewObject glVO = glIter.getViewObject();
          
           glVO.setNamedWhereClauseParam("pTempValue", new oracle.jbo.domain.Number(1));
           glVO.setNamedWhereClauseParam("chartOfAccountId", ADFContext.getCurrent().getSessionScope().get("ChartOfAccountsId"));
           glVO.setNamedWhereClauseParam("seg1", holdKffRow.getAttribute("Segment1"));
           glVO.setNamedWhereClauseParam("seg2", holdKffRow.getAttribute("Segment2"));
           glVO.setNamedWhereClauseParam("seg3", holdKffRow.getAttribute("Segment3"));
           glVO.setNamedWhereClauseParam("seg4", holdKffRow.getAttribute("Segment4"));
           glVO.setNamedWhereClauseParam("seg5", holdKffRow.getAttribute("Segment5"));
           glVO.setNamedWhereClauseParam("seg6", holdKffRow.getAttribute("Segment6"));
           glVO.setNamedWhereClauseParam("seg7", holdKffRow.getAttribute("Segment7"));
           glVO.setNamedWhereClauseParam("seg8", holdKffRow.getAttribute("Segment8"));
           glVO.setNamedWhereClauseParam("seg9", holdKffRow.getAttribute("Segment9"));
           glVO.setNamedWhereClauseParam("seg10", holdKffRow.getAttribute("Segment10"));
           
           glVO.executeQuery();
           System.out.println("after searching gl: "+glVO.getRowCount());
       }
   
    public void searchGLCode(ActionEvent actionEvent) {
        // Add event code here...
        queryGLCode();
    }

    public void createGLCodeCombination(ActionEvent actionEvent) {
        // Add event code here...

        FacesContext facesContext = FacesContext.getCurrentInstance();
        org.apache.myfaces.trinidad.render.ExtendedRenderKitService service = org.apache.myfaces.trinidad.util.Service.getRenderKitService(facesContext, ExtendedRenderKitService.class);
        StringBuffer jsScript = new StringBuffer();
        StringBuffer errorMessage = new StringBuffer();
        StringBuffer segments = new StringBuffer();
        boolean segmentMissing = false;
        
        DCIteratorBinding holdKffIter = ADFUtils.findIterator("HoldKffVO1Iterator");
        Row holdKffRow = holdKffIter.getCurrentRow();
        
        if (holdKffRow == null)
          return;
        
        if (holdKffRow.getAttribute("Segment1") == null)
        {
                segmentMissing = true;
                errorMessage.append(ADFContext.getCurrent().getSessionScope().get("segment1Prompt"));
            }
        
        if (holdKffRow.getAttribute("Segment2") == null)
        {
                segmentMissing = true;
                errorMessage.append(ADFContext.getCurrent().getSessionScope().get("segment2Prompt"));
            }

        if (holdKffRow.getAttribute("Segment3") == null)
        {
                segmentMissing = true;
                errorMessage.append(ADFContext.getCurrent().getSessionScope().get("segment3Prompt"));
            }

        if (holdKffRow.getAttribute("Segment4") == null)
        {
                segmentMissing = true;
                errorMessage.append(ADFContext.getCurrent().getSessionScope().get("segment4Prompt"));
            }

        if (holdKffRow.getAttribute("Segment5") == null)
        {
                segmentMissing = true;
                errorMessage.append(ADFContext.getCurrent().getSessionScope().get("segment5Prompt"));
            }

        if (holdKffRow.getAttribute("Segment6") == null)
        {
                segmentMissing = true;
                errorMessage.append(ADFContext.getCurrent().getSessionScope().get("segment6Prompt"));
            }

        if (holdKffRow.getAttribute("Segment7") == null)
        {
                segmentMissing = true;
                errorMessage.append(ADFContext.getCurrent().getSessionScope().get("segment7Prompt"));
            }

        if (holdKffRow.getAttribute("Segment8") == null)
        {
                segmentMissing = true;
                errorMessage.append(ADFContext.getCurrent().getSessionScope().get("segment8Prompt"));
            }

        if (holdKffRow.getAttribute("Segment9") == null)
        {
                segmentMissing = true;
                errorMessage.append(ADFContext.getCurrent().getSessionScope().get("segment9Prompt"));
            }

        if (holdKffRow.getAttribute("Segment10") == null)
        {
                segmentMissing = true;
                errorMessage.append(ADFContext.getCurrent().getSessionScope().get("segment10Prompt"));
            }

        if (segmentMissing)
        {

            
            jsScript.append("showMessage('Mandatory Fields','"+errorMessage.toString()+"');");
            service.addScript(facesContext, jsScript.toString());
            return;
        } 
        
        segments.append(holdKffRow.getAttribute("Segment1")+"-");
        segments.append(holdKffRow.getAttribute("Segment2")+"-");
        segments.append(holdKffRow.getAttribute("Segment3")+"-");
        segments.append(holdKffRow.getAttribute("Segment4")+"-");
        segments.append(holdKffRow.getAttribute("Segment5")+"-");
        segments.append(holdKffRow.getAttribute("Segment6")+"-");
        segments.append(holdKffRow.getAttribute("Segment7")+"-");
        segments.append(holdKffRow.getAttribute("Segment8")+"-");
        segments.append(holdKffRow.getAttribute("Segment9")+"-");
        segments.append(holdKffRow.getAttribute("Segment10"));
        
        OperationBinding createGLComb = ADFUtils.findOperation("createGLCodeCombination");
                Map createGLCombParam = createGLComb.getParamsMap();
                createGLCombParam.put("pApplCode",ADFContext.getCurrent().getSessionScope().get("ApplicationShortName"));
                createGLCombParam.put("pKeyFlexCode",ADFContext.getCurrent().getSessionScope().get("IdFlexCode"));
                createGLCombParam.put("pIdFlexNum",ADFContext.getCurrent().getSessionScope().get("ChartOfAccountsId"));
                createGLCombParam.put("pSegments",segments.toString());
                String result = (String)createGLComb.execute();
               
                if (!result.equalsIgnoreCase("Y"))
                {
                        RichPopup popup = this.getKffPopup();
                        popup.hide();
                        
                        jsScript.append("showMessage('','"+result+"');");
                        service.addScript(facesContext, jsScript.toString());
                        return;
                    }
                
        queryGLCode();
    }

    public void selectGlCodeCombination(ActionEvent actionEvent) {
        // Add event code here...
        
        DCIteratorBinding glCodeItem = ADFUtils.findIterator("GLCodeCombinationsVO1Iterator");
        if (glCodeItem == null)
          return;
        
        Row glCodeRow = glCodeItem.getCurrentRow();
        if (glCodeRow == null)
          return;
         
        DCIteratorBinding distIter = ADFUtils.findIterator("DistributionsEO1Iterator");
        //if (distIter == null)
        //  return;
        
        Row distRow = distIter.getCurrentRow();
        //if (distRow == null)
        // return;
        
        distRow.setAttribute("Ccid", glCodeRow.getAttribute("CodeCombinationId"));
        distRow.setAttribute("ConcatenatedSegments", glCodeRow.getAttribute("ConcatenatedSegments"));
        distRow.setAttribute("AccountDescription", glCodeRow.getAttribute("AccountDescription"));
        distRow.setAttribute("LedgerId", glCodeRow.getAttribute("ChartOfAccountsId"));
        
        RichPopup popup = this.getKffPopup();
        popup.hide();
    }

    public void clearCodeCombinations(ActionEvent actionEvent) {
        // Add event code here...
        
        DCIteratorBinding holdKffIter = ADFUtils.findIterator("HoldKffVO1Iterator");
        ViewObject kffVO = holdKffIter.getViewObject();
        kffVO.executeQuery();
        
        DCIteratorBinding glCodeItem = ADFUtils.findIterator("GLCodeCombinationsVO1Iterator");
        ViewObject glVO = glCodeItem.getViewObject();
        glVO.executeEmptyRowSet();
    }

    public void cancelTransaction(ActionEvent actionEvent) {
        // Add event code here...
        
                DCIteratorBinding headIter = ADFUtils.findIterator("TrxHeadersVO1Iterator");
                Row headRow = headIter.getCurrentRow();
                
                DCIteratorBinding lineIter = ADFUtils.findIterator("TrxLinesVO1Iterator");
                //Row lineRow = lineIter.getCurrentRow();

                DCIteratorBinding distIter = ADFUtils.findIterator("DistributionsEO1Iterator");
                //Row hdistRow = distIter.getCurrentRow();

                if (headRow != null)
                {
                    if (headRow.getAttribute("RowStatus") != null)
                    {
                        int i = ((Integer)headRow.getAttribute("RowStatus")).intValue();
                      
                        if (i == 0)
                        {
                            lineIter.removeCurrentRow();
                            headRow.remove();
                        }
                        else if (i == 2)
                            headRow.refresh(Row.REFRESH_UNDO_CHANGES | Row.REFRESH_WITH_DB_FORGET_CHANGES);
                        }
                    }
    }

    public void submitRequest(ActionEvent actionEvent) {
        // Add event code here...
        
        FacesContext facesContext = FacesContext.getCurrentInstance();
        org.apache.myfaces.trinidad.render.ExtendedRenderKitService service = org.apache.myfaces.trinidad.util.Service.getRenderKitService(facesContext, ExtendedRenderKitService.class);
        StringBuffer jsScript = new StringBuffer();
        String status = "";

        status = validateHeader();
        if (!"0".equalsIgnoreCase(status))
        {
//                jsScript.append("showMessage('','"+status+"');");
                showSweetAlertHtmlmessage("Sales Invoice", status, "error");
                service.addScript(facesContext, jsScript.toString());
                return;
            }
            
        status = validateLines();
        if (!"0".equalsIgnoreCase(status))
        {
//                jsScript.append("showMessage('','"+status+"');");
                showSweetAlertHtmlmessage("Sales Invoice", status, "error");
                service.addScript(facesContext, jsScript.toString());
                return;
            }
        
        OperationBinding validateDist = ADFUtils.findOperation("validateDist");

        Object statusObj = validateDist.execute();
        status = statusObj.toString();
        List errors = validateDist.getErrors();
//        System.out.println("error count: "+errors.size());
        
//        for (int i =0; i <errors.size(); i++ )
//        {
//            System.out.println("error :"+i+ "  "+errors.get(i));
//            }
        
        if (!"0".equalsIgnoreCase(status))
        {
//                jsScript.append("showMessage('','"+status+"');");
                showSweetAlertHtmlmessage("Sales Invoice", status, "error");
                service.addScript(facesContext, jsScript.toString());
                return;
            }
        
        OperationBinding validateAttachment = ADFUtils.findOperation("validateAttachment");

        Object attachObj = validateAttachment.execute();
        status = attachObj.toString();
        if (!"0".equalsIgnoreCase(status))
        {
//                jsScript.append("showMessage('','"+status+"');");
                showSweetAlertHtmlmessage("Sales Invoice", status, "error");
                service.addScript(facesContext, jsScript.toString());
                return;
            }
        
        DCIteratorBinding headIter = ADFUtils.findIterator("TrxHeadersVO1Iterator");
        Row headerRow = headIter.getCurrentRow();
        
        if (headerRow.getAttribute("TrackingMonth") != null )
        {
                OperationBinding validateTrackingMonth = ADFUtils.findOperation("ExecuteWithParams");
                        Map validateTrackingMonthParam = validateTrackingMonth.getParamsMap();
                        validateTrackingMonthParam.put("pTxnHeaderId",headerRow.getAttribute("TxnHeaderId"));
                        validateTrackingMonthParam.put("pMonths",headerRow.getAttribute("TrackingMonth"));
                        validateTrackingMonth.execute();
                
                DCIteratorBinding trackingMonthIterator = ADFUtils.findIterator("ValidateTrackingMonthVO1Iterator");
                Row trackingMonthRow = trackingMonthIterator.getRowAtRangeIndex(0);
                
                if (trackingMonthRow != null)
                {
                RichPopup.PopupHints hints = new RichPopup.PopupHints();
                    this.getTrackingMonthPopup().show(hints);
                }
                
            }
    }
    
    public List<SelectItem> suggestTaxCode(String pTaxCode) {
        // Add event code here...
       
        List<SelectItem> documentList = new ArrayList<SelectItem>();
                if (pTaxCode != null) {
                    BindingContext bindingctx = BindingContext.getCurrent();
                    BindingContainer bindingcnt = bindingctx.getCurrentBindingsEntry();
                    DCIteratorBinding taxIter = (DCIteratorBinding) bindingcnt.get("TaxCodeVO1Iterator");
                    
                    DCIteratorBinding lineIter = (DCIteratorBinding) bindingcnt.get("TrxLinesVO1Iterator");
                    Row lineRow = lineIter.getCurrentRow();
                    ViewObject taxVO = taxIter.getViewObject();
                    
                    //customerVO.clearCache();
                    //taxVO.setNamedWhereClauseParam("pOrgId", headerRow.getAttribute("OrgId"));
                    taxVO.setWhereClause("upper(tax_rate_code) like upper('"+pTaxCode+"%')");
                    taxVO.executeQuery();
                    
                    System.out.println("..Row count:"+taxVO.getRowCount());
                    int count = 0;
                    while (taxVO.hasNext()) {
                        Row row = taxVO.next();
                       
                        String matchingEntries = (String) row.getAttribute("TaxRateCode");
                        if (matchingEntries.toUpperCase().startsWith(pTaxCode.toUpperCase())) {
                        //if (pVendorName.toUpperCase().contains(matchingEntries.toUpperCase())) {
                            documentList.add(new SelectItem(matchingEntries, matchingEntries));
              
                            count = count + 1;
                                if (count > 4)
                                    break;
                        }
                    }
                }
        return documentList;
    }

    public void addNewDistRow(ActionEvent actionEvent) {
        // Add event code here...
        OperationBinding createInsert = ADFUtils.findOperation("CreateInsert1");
        createInsert.execute();
        DCIteratorBinding distIter = ADFUtils.findIterator("DistributionsEO1Iterator");
        
        Row taxRow=null;
        taxRow = distIter.getCurrentRow(); // iterator.createRow();
        taxRow.setAttribute("LineType", "LINE");
        distIter.executeQuery();
    }

    public void saveRequest(ActionEvent actionEvent) {
        // Add event code here...
       System.out.println("inside save request");
        FacesContext facesContext = FacesContext.getCurrentInstance();
        org.apache.myfaces.trinidad.render.ExtendedRenderKitService service = org.apache.myfaces.trinidad.util.Service.getRenderKitService(facesContext, ExtendedRenderKitService.class);
        StringBuffer jsScript = new StringBuffer();
        String status = "";
        
        OperationBinding commit = ADFUtils.findOperation("Commit");
        commit.execute();
        System.out.println("testing");
        showSweetAlertHtmlmessage("Sales Invoice", "<li><span>Saved Successfully</span></li>", "success");
    }
    
    public void returnToNotification(ActionEvent actionEvent) {
        // Add event code here...
       System.out.println("inside returnToNotification");
        FacesContext facesContext = FacesContext.getCurrentInstance();
        org.apache.myfaces.trinidad.render.ExtendedRenderKitService service = org.apache.myfaces.trinidad.util.Service.getRenderKitService(facesContext, ExtendedRenderKitService.class);
        StringBuffer jsScript = new StringBuffer();
        String status = "";
        
        OperationBinding rollback = ADFUtils.findOperation("Rollback");
        rollback.execute();

        try{
        ExternalContext ectx = FacesContext.getCurrentInstance().getExternalContext();
            
           HttpServletResponse response = (HttpServletResponse)ectx.getResponse();
           HttpSession session = (HttpSession)ectx.getSession(false);

           
            Object logoutUrlObj = session.getAttribute("LogOutUrl");
            String logoutUrl = "";
            
            if (logoutUrlObj != null)
                logoutUrl = (String)logoutUrlObj;
            System.out.println("ADFContext.getCurrent().getSessionScope().get(\"NotificationId\"):"+ADFContext.getCurrent().getSessionScope().get("NotificationId"));
            
            if (ADFContext.getCurrent().getSessionScope().get("NotificationId") == null)
            {
                    showSweetAlertHtmlmessage("Sales Invoice", "<li><span>Kindly visit this page from notification.</span></li>", "error");
                    return;
                }
            String  notId = (String)ADFContext.getCurrent().getSessionScope().get("NotificationId");
            session.invalidate();
            
           response.sendRedirect(logoutUrl+"/OA_HTML/OA.jsp?OAFunc=FND_WFNTF_DETAILS&NtfId="+notId+"&addBreadCrumb=Y&retainAM=Y");
            
            FacesContext.getCurrentInstance().responseComplete();
            
          
        }catch(Exception e)
        {
            System.out.println("exception occurred:"+e);
            }
    }

    public void onOperatingUintChange(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
       
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        Object orgId =  resolveExpression("#{bindings.OrgId.inputValue}");
        
        DCIteratorBinding lineIter = ADFUtils.findIterator("TrxLinesVO1Iterator");
        int lineCount = lineIter.getViewObject().getRowCount();
        RowSetIterator iterator = lineIter.getRowSetIterator();
        
        for(int i=0; i<lineCount; i++)
        {
                Row lineRow = iterator.getRowAtRangeIndex(i);
                lineRow.setAttribute("OrgId", orgId);
        }
        iterator.closeRowSetIterator();
    }
    
    
    
    

    public void setDistPopup(RichPopup distPopup) {
        this.distPopup = distPopup;
    }

    public RichPopup getDistPopup() {
        return distPopup;
    }

    public void cancelDistPopup(ActionEvent actionEvent) {
        // Add event code here...
        RichPopup popup = this.getDistPopup();
        popup.hide();
    }
    
    public void filterHistory(ActionEvent actionEvent) {
        // Add event code here...
                System.out.println("inside filterHistory");

                BindingContext bindingctx = BindingContext.getCurrent();
                BindingContainer bindingcnt = bindingctx.getCurrentBindingsEntry();
                DCIteratorBinding searchIter = (DCIteratorBinding) bindingcnt.get("SearchhistoryVO1Iterator");

                Row r =searchIter.getCurrentRow();
                System.out.println("CustomerName"+r.getAttribute("CustomerName"));
                System.out.println("CustomerId"+r.getAttribute("CustomerId"));
                System.out.println("OperatingUnits"+r.getAttribute("OperatingUnits"));
                System.out.println("LineDescription"+r.getAttribute("LineDescription"));
                System.out.println("RequesterName"+r.getAttribute("RequesterName"));
                System.out.println("RequesterEmpId"+r.getAttribute("RequesterEmpId"));
                System.out.println("RequesterEmpCode"+r.getAttribute("RequesterEmpCode"));

                //System.out.println("selected "+this.getSelectedVendorName());
                OperationBinding query = ADFUtils.findOperation("queryHistory");
                   
                Map queryParam = query.getParamsMap();
                queryParam.put("pCustomerId",r.getAttribute("CustomerId"));
                queryParam.put("pOperatingUnits",r.getAttribute("OperatingUnits"));
                queryParam.put("pLineDescription",r.getAttribute("LineDescription"));
                queryParam.put("pRequesterEmpId",r.getAttribute("RequesterEmpId"));
                queryParam.put("pCustomerName",r.getAttribute("CustomerName"));
                queryParam.put("pRequesterName",r.getAttribute("RequesterName"));
                queryParam.put("pRequesterEmpNumber",r.getAttribute("RequesterEmpCode"));
                queryParam.put("pGlDate",r.getAttribute("GlDate"));
                queryParam.put("pInvoiceDate",r.getAttribute("InvoiceDate"));
                queryParam.put("pCollectionStatus",r.getAttribute("CollectionStatus"));
                          
                query.execute();
    }
    
    public void filterCustomer(ActionEvent actionEvent) {
        // Add event code here...
        System.out.println("inside filterCustomer");

                BindingContext bindingctx = BindingContext.getCurrent();
                BindingContainer bindingcnt = bindingctx.getCurrentBindingsEntry();
                DCIteratorBinding searchIter = (DCIteratorBinding) bindingcnt.get("SearchCustomerVO1Iterator");

                Row r =searchIter.getCurrentRow();
                System.out.println("CustomerName"+r.getAttribute("CustomerName"));
                System.out.println("CustomerNumber"+r.getAttribute("CustomerNumber"));
                System.out.println("Contact"+r.getAttribute("Contact"));
                System.out.println("TermName"+r.getAttribute("TermName"));
                   //System.out.println("selected "+this.getSelectedVendorName());
                   OperationBinding query = ADFUtils.findOperation("queryCustomer");
                   
                Map queryParam = query.getParamsMap();
                queryParam.put("pCustomerName",r.getAttribute("CustomerName"));
                queryParam.put("pCustomerNumber",r.getAttribute("CustomerNumber"));
                queryParam.put("pContact",r.getAttribute("Contact"));
                queryParam.put("pTermName",r.getAttribute("TermName"));
                query.execute();
    }
//
    public String submitAction() {
        // Add event code here...

        FacesContext facesContext = FacesContext.getCurrentInstance();
        org.apache.myfaces.trinidad.render.ExtendedRenderKitService service = org.apache.myfaces.trinidad.util.Service.getRenderKitService(facesContext, ExtendedRenderKitService.class);
        StringBuffer jsScript = new StringBuffer();
        String status = "";

        status = validateHeader();
        if (!"0".equalsIgnoreCase(status))
        {
        //                jsScript.append("showMessage('','"+status+"');");
                showSweetAlertHtmlmessage("Sales Invoice", status, "error");
                service.addScript(facesContext, jsScript.toString());
                return null;
            }
            
        status = validateLines();
        if (!"0".equalsIgnoreCase(status))
        {
        //                jsScript.append("showMessage('','"+status+"');");
                showSweetAlertHtmlmessage("Sales Invoice", status, "error");
                service.addScript(facesContext, jsScript.toString());
                return null;
            }
        
        OperationBinding validateDist = ADFUtils.findOperation("validateDist");
        

        Object statusObj = validateDist.execute();
        status = statusObj.toString();
        List errors = validateDist.getErrors();
        System.out.println("error count: "+errors.size());
        
//        for (int i =0; i <errors.size(); i++ )
//        {
//            System.out.println("error :"+i+ "  "+errors.get(i));
//            }
        
        if (!"0".equalsIgnoreCase(status))
        {
        //                jsScript.append("showMessage('','"+status+"');");
                showSweetAlertHtmlmessage("Sales Invoice", status, "error");
                service.addScript(facesContext, jsScript.toString());
                return null;
            }
        
        OperationBinding validateAttachment = ADFUtils.findOperation("validateAttachment");

        Object attachObj = validateAttachment.execute();
        status = attachObj.toString();
        if (!"0".equalsIgnoreCase(status))
        {
        //                jsScript.append("showMessage('','"+status+"');");
                showSweetAlertHtmlmessage("Sales Invoice", status, "error");
                service.addScript(facesContext, jsScript.toString());
                return null;
            }
        BindingContext bindingctx = BindingContext.getCurrent();
        BindingContainer bindingcnt = bindingctx.getCurrentBindingsEntry();
        DCIteratorBinding headerIter = (DCIteratorBinding) bindingcnt.get("TrxHeadersVO1Iterator");

        Row r =headerIter.getCurrentRow();
        System.out.println("r.getAttribute(\"TrackingMonth\"): "+r.getAttribute("TrackingMonth"));
        if (r.getAttribute("TrackingMonth") != null )
        {
                OperationBinding validateTrackingMonth = ADFUtils.findOperation("ExecuteWithParams");
                        Map validateTrackingMonthParam = validateTrackingMonth.getParamsMap();
                        validateTrackingMonthParam.put("pTxnHeaderId",r.getAttribute("TxnHeaderId"));
                        validateTrackingMonthParam.put("pMonths",r.getAttribute("TrackingMonth"));
                        validateTrackingMonth.execute();
                
                DCIteratorBinding trackingMonthIterator = ADFUtils.findIterator("ValidateTrackingMonthVO1Iterator");
                Row trackingMonthRow = trackingMonthIterator.getRowAtRangeIndex(0);
                
                if (trackingMonthRow != null)
                {
                    System.out.println("popping up popup window");
                RichPopup.PopupHints hints = new RichPopup.PopupHints();
                    this.getTrackingMonthPopup().show(hints);
                    return null;
                }
            }
        
        OperationBinding callWorkflow = ADFUtils.findOperation("callWorkflow");
        Map paramMap = callWorkflow.getParamsMap();
        paramMap.put("pTxnHeaderId", r.getAttribute("TxnHeaderId"));
        paramMap.put("pUserId", ADFContext.getCurrent().getSessionScope().get("UserId"));
        paramMap.put("pResponsibilityId", ADFContext.getCurrent().getSessionScope().get("RespId"));
        Object obj = callWorkflow.execute();
        
        if (obj == null)
        {
                        showSweetAlertHtmlmessage("Sales Invoice", "<li><span>After submitting workflow no status returned. Kindly check.</span></li>", "error");
                        service.addScript(facesContext, jsScript.toString());
                        return null;
            }
        else if (!"0".equalsIgnoreCase((String)obj))
        {
                        showSweetAlertHtmlmessage("Sales Invoice", "<li><span>"+(String)obj+"</span></li>", "error");
                        service.addScript(facesContext, jsScript.toString());
                        return null;
            }
        
        
        
        setValueToEL("#{bindings.Status.inputValue}", "SUBMITTED"); //IN PROCESS
        OperationBinding commit = ADFUtils.findOperation("Commit");
        commit.execute();

        showSweetAlertHtmlmessage("Sales Invoice", "<li><span>Invoice submitted for approval</span></li>", "success");
        service.addScript(facesContext, jsScript.toString());

        return "toSearch";
    }

    public void setWithdrawPopup(RichPopup withdrawPopup) {
        this.withdrawPopup = withdrawPopup;
    }

    public RichPopup getWithdrawPopup() {
        return withdrawPopup;
    }

    public void reviewComplete(ActionEvent actionEvent) {
        // Add event code here...
        FacesContext facesContext = FacesContext.getCurrentInstance();
        org.apache.myfaces.trinidad.render.ExtendedRenderKitService service = org.apache.myfaces.trinidad.util.Service.getRenderKitService(facesContext, ExtendedRenderKitService.class);
        StringBuffer jsScript = new StringBuffer();
        String status = "";

        status = validateHeader();
        if (!"0".equalsIgnoreCase(status))
        {
        //                jsScript.append("showMessage('','"+status+"');");
                showSweetAlertHtmlmessage("Sales Invoice", status, "error");
                service.addScript(facesContext, jsScript.toString());
                return;
            }
            
        status = validateLines();
        if (!"0".equalsIgnoreCase(status))
        {
        //                jsScript.append("showMessage('','"+status+"');");
                showSweetAlertHtmlmessage("Sales Invoice", status, "error");
                service.addScript(facesContext, jsScript.toString());
                return;
            }
        
        OperationBinding validateDist = ADFUtils.findOperation("validateDist");

        Object statusObj = validateDist.execute();
        status = statusObj.toString();
        List errors = validateDist.getErrors();
        System.out.println("error count: "+errors.size());
        
//        for (int i =0; i <errors.size(); i++ )
//        {
//            System.out.println("error :"+i+ "  "+errors.get(i));
//            }
        
        if (!"0".equalsIgnoreCase(status))
        {
        //                jsScript.append("showMessage('','"+status+"');");
                showSweetAlertHtmlmessage("Sales Invoice", status, "error");
                service.addScript(facesContext, jsScript.toString());
                return;
            }
        
        OperationBinding validateAttachment = ADFUtils.findOperation("validateAttachment");

        Object attachObj = validateAttachment.execute();
        status = attachObj.toString();
        if (!"0".equalsIgnoreCase(status))
        {
        //                jsScript.append("showMessage('','"+status+"');");
                showSweetAlertHtmlmessage("Sales Invoice", status, "error");
                service.addScript(facesContext, jsScript.toString());
                return;
            }

        OperationBinding commit = ADFUtils.findOperation("Commit");
        commit.execute();
        
        showSweetAlertHtmlmessage("Sales Invoice", "<li><span>Review Completed</span></li>", "success");
        service.addScript(facesContext, jsScript.toString());

    }
    
    public String reviewComplete() {
        // Add event code here...
        FacesContext facesContext = FacesContext.getCurrentInstance();
        org.apache.myfaces.trinidad.render.ExtendedRenderKitService service = org.apache.myfaces.trinidad.util.Service.getRenderKitService(facesContext, ExtendedRenderKitService.class);
        StringBuffer jsScript = new StringBuffer();
        String status = "";

        status = validateHeader();
        if (!"0".equalsIgnoreCase(status))
        {
        //                jsScript.append("showMessage('','"+status+"');");
                showSweetAlertHtmlmessage("Sales Invoice", status, "error");
                service.addScript(facesContext, jsScript.toString());
                return null;
            }
            
        status = validateLines();
        if (!"0".equalsIgnoreCase(status))
        {
        //                jsScript.append("showMessage('','"+status+"');");
                showSweetAlertHtmlmessage("Sales Invoice", status, "error");
                service.addScript(facesContext, jsScript.toString());
                return null;
            }
        
        OperationBinding validateDist = ADFUtils.findOperation("validateDist");

        Object statusObj = validateDist.execute();
        status = statusObj.toString();
        List errors = validateDist.getErrors();
//        System.out.println("error count: "+errors.size());
//        
//        for (int i =0; i <errors.size(); i++ )
//        {
//            System.out.println("error :"+i+ "  "+errors.get(i));
//            }
        
        if (!"0".equalsIgnoreCase(status))
        {
        //                jsScript.append("showMessage('','"+status+"');");
                showSweetAlertHtmlmessage("Sales Invoice", status, "error");
                service.addScript(facesContext, jsScript.toString());
                return null;
            }
        
        OperationBinding validateAttachment = ADFUtils.findOperation("validateAttachment");

        Object attachObj = validateAttachment.execute();
        status = attachObj.toString();
        if (!"0".equalsIgnoreCase(status))
        {
        //                jsScript.append("showMessage('','"+status+"');");
                showSweetAlertHtmlmessage("Sales Invoice", status, "error");
                service.addScript(facesContext, jsScript.toString());
                return null;
            }

        OperationBinding commit = ADFUtils.findOperation("Commit");
        commit.execute();
        
        showSweetAlertHtmlmessage("Sales Invoice", "<li><span>Review Completed</span></li>", "success");
        service.addScript(facesContext, jsScript.toString());
        Object accessFromObj = ADFContext.getCurrent().getSessionScope().get("AccessFrom");
        
        String accessFrom = "APPL";
        if (accessFromObj != null)
            accessFrom = (String)accessFromObj;
        
        if ("WF".equalsIgnoreCase(accessFrom))
        {
                
                try{
                ExternalContext ectx = FacesContext.getCurrentInstance().getExternalContext();
                    
                   HttpServletResponse response = (HttpServletResponse)ectx.getResponse();
                   HttpSession session = (HttpSession)ectx.getSession(false);

                   
                    Object logoutUrlObj = session.getAttribute("LogOutUrl");
                    String logoutUrl = "";
                    
                    if (logoutUrlObj != null)
                        logoutUrl = (String)logoutUrlObj;
                    System.out.println("ADFContext.getCurrent().getSessionScope().get(\"NotificationId\"):"+ADFContext.getCurrent().getSessionScope().get("NotificationId"));
                    
                    if (ADFContext.getCurrent().getSessionScope().get("NotificationId") == null)
                    {
                            showSweetAlertHtmlmessage("Sales Invoice", "<li><span>Kindly visit this page from notification.</span></li>", "error");
                            return null;
                        }
                    String  notId = (String)ADFContext.getCurrent().getSessionScope().get("NotificationId");
                    session.invalidate();
                    
                   response.sendRedirect(logoutUrl+"/OA_HTML/OA.jsp?OAFunc=FND_WFNTF_DETAILS&NtfId="+notId+"&addBreadCrumb=Y&retainAM=Y");
                    
                    FacesContext.getCurrentInstance().responseComplete();
                    
                  
                }catch(Exception e)
                {
                    System.out.println("exception occurred:"+e);
                    }
                
            }
        
        return "toSearch";
    }

    public String importAction() {
        // Add event code here...
        //logic to import the invoice into interface and run the interface program (if possible) and then go back to history page

        BindingContext bindingctx = BindingContext.getCurrent();
        BindingContainer bindingcnt = bindingctx.getCurrentBindingsEntry();
        DCBindingContainer bindingImple = (DCBindingContainer) bindingcnt;
        DCIteratorBinding headerIter = bindingImple.findIteratorBinding("TrxHeadersVO1Iterator");
            
        Row row = headerIter.getCurrentRow();
        
        OperationBinding  interfaceInvoice = ADFUtils.findOperation("interfaceInvoice");
        Map param = interfaceInvoice.getParamsMap();
        param.put("pTxnHeaderId", row.getAttribute("TxnHeaderId"));
        String retStatus = (String)interfaceInvoice.execute();
        
        List errorList = interfaceInvoice.getErrors();
        int errorCount = errorList.size();
        if (errorCount > 0)
        {
                   StringBuffer str = new StringBuffer();
                   for (int i=0; i<errorCount;i++)
                           str.append("<li><span>"+errorList.get(i)+"</span></li>");

               showSweetAlertHtmlmessage("Sales Invoice", str.toString(), "error");
               return null;
            }
        
        if (!"0".equalsIgnoreCase(retStatus))
        {
                showSweetAlertHtmlmessage("Sales Invoice", "<li><span>"+retStatus+"</span></li>", "error");
                return null;
            }
           else if ("0".equalsIgnoreCase(retStatus))
        {
                setValueToEL("#{bindings.Status.inputValue}", "INTERFACED");
                OperationBinding commit = ADFUtils.findOperation("Commit");
                commit.execute();
            }
        
        showSweetAlertHtmlmessage("Sales Invoice", "<li><span>Invoice interfaced successfully</span></li>", "success");
        return "toSearch";
    }

    public void queryActionHistory(ActionEvent actionEvent) {
        // Add event code here...
        
        System.out.println("start of queryActionHistory");
        BindingContext bindingctx = BindingContext.getCurrent();
        BindingContainer bindingcnt = bindingctx.getCurrentBindingsEntry();
        DCBindingContainer bindingImple = (DCBindingContainer) bindingcnt;
        DCIteratorBinding historyIter = bindingImple.findIteratorBinding("HistoryVO1Iterator");
        System.out.println("s2");
        Row row = historyIter.getCurrentRow();
        System.out.println("current row.getAttribute(\"TxnHeaderId\")"+row.getAttribute("TxnHeaderId"));
        OperationBinding  itemKeyOper = ADFUtils.findOperation("getItemKey");
        System.out.println("s3");
        Map param = itemKeyOper.getParamsMap();
        System.out.println("s4");
        param.put("pTxnHeaderId", row.getAttribute("TxnHeaderId"));
        param.put("pEntityName", "XSALINV");
        String itemKey = (String)itemKeyOper.execute();
        
        
       if (itemKey != null)
       {
        
        if (itemKey.contains("Error"))
        {
                showSweetAlertHtmlmessage("Sales Invoice", "<li><span>"+itemKey+"</span></li>", "error");
                return;
            }
       }
        
        System.out.println("itemKey:"+itemKey);
        List errorList = itemKeyOper.getErrors();
        int errorCount = errorList.size();
        if (errorCount > 0)
        {
                System.out.println("inside error 1");
                   StringBuffer str = new StringBuffer();
                   for (int i=0; i<errorCount;i++)
                           str.append("<li><span>"+errorList.get(i)+"</span></li>");
                
               showSweetAlertHtmlmessage("Sales Invoice", str.toString(), "error");
               return;
            }
        
            System.out.println("querying action history now");
                OperationBinding  executeOper = ADFUtils.findOperation("ExecuteWithParams");
                Map executeParam = executeOper.getParamsMap();
                executeParam.put("ITEM_TYPE", "XSALINV");
                executeParam.put("ITEM_KEY", itemKey);
                executeOper.execute();
                
                List executeErrorList = executeOper.getErrors();
                int executeErrorCount = executeErrorList.size();
                if (executeErrorCount > 0)
                {
                        System.out.println("inside error 2");
                           StringBuffer str = new StringBuffer();
                           for (int i=0; i<executeErrorCount;i++)
                                   str.append("<li><span>"+executeErrorList.get(i)+"</span></li>");

                       showSweetAlertHtmlmessage("Sales Invoice", str.toString(), "error");
                       return;
                    }
                DCIteratorBinding actionHistoryIter = bindingImple.findIteratorBinding("WfActionHistoryVO1Iterator");
                
        return;
    }

    public void withdrawInvoice(ActionEvent actionEvent) {
        // Add event code here...
        
        DCIteratorBinding headerIter = ADFUtils.findIterator("TrxHeadersVO1Iterator");
        Row headerRow = headerIter.getCurrentRow();
        
        OperationBinding  withdraw = ADFUtils.findOperation("withdrawInvoice");
        Map param = withdraw.getParamsMap();
        param.put("pTxnHeaderId", headerRow.getAttribute("TxnHeaderId"));
        String retStatus = (String)withdraw.execute();
        System.out.println("withdraw retStatus:"+retStatus);
        StringBuffer str = new StringBuffer();
        if(!"0".equalsIgnoreCase(retStatus))
        {
                str.append("<li><span>"+retStatus+"</span></li>");
                showSweetAlertHtmlmessage("Sales Invoice", str.toString(), "error");
                return;
            }
        
        List errorList = withdraw.getErrors();
        int errorCount = errorList.size();
        
        if (errorCount > 0)
        {
                   
                   for (int i=0; i<errorCount;i++)
                           str.append("<li><span>"+errorList.get(i)+"</span></li>");

               showSweetAlertHtmlmessage("Sales Invoice", str.toString(), "error");
               return;
            }
        
        
        OperationBinding  createRow = ADFUtils.findOperation("CreateInsertWithdraw");
        createRow.execute();
        BindingContext bindingctx = BindingContext.getCurrent();
        BindingContainer bindingcnt = bindingctx.getCurrentBindingsEntry();
        DCBindingContainer bindingImple = (DCBindingContainer) bindingcnt;
        DCIteratorBinding withdrawIter = bindingImple.findIteratorBinding("WithdrawlVO1Iterator");
            
        Row row = withdrawIter.getCurrentRow();
        row.setAttribute("LastUpdateLogin", ADFContext.getCurrent().getSessionScope().get("LoginId"));

        OperationBinding  findItemKey = ADFUtils.findOperation("CreateInsertWithdraw");
        
        Map queryParam = findItemKey.getParamsMap();
        queryParam.put("pTxnHeaderId",headerRow.getAttribute("TxnHeaderId"));
        queryParam.put("pEntityName","XSALINV");
        Object itemKeyObj = findItemKey.execute();
        List errors = findItemKey.getErrors();
        int k = errors.size();
            if ( k > 0)
            {
//                    System.out.println("inside error 2");
                       StringBuffer str2 = new StringBuffer();
                       for (int i=0; i<k;i++)
                               str2.append("<li><span>"+errors.get(i)+"</span></li>");

                   showSweetAlertHtmlmessage("Sales Invoice", str2.toString(), "error");
                   return;
                }
        if (itemKeyObj == null)
            row.setAttribute("ItemKey", null);
        else
            row.setAttribute("ItemKey", (String)itemKeyObj); 
        
        setValueToEL("#{bindings.Status.inputValue}", "DRAFT");
        OperationBinding commit = ADFUtils.findOperation("Commit");
        commit.execute();
        
        ADFContext.getCurrent().getPageFlowScope().put("action", "USER_EDIT");
        this.getWithdrawPopup().hide();
        
        showSweetAlertHtmlmessage("Sales Invoice", "<li><span>Invoice has been withdrawn.</span></li>", "success");
        
    }

    public void setTrackingMonthPopup(RichPopup trackingMonthPopup) {
        this.trackingMonthPopup = trackingMonthPopup;
    }

    public RichPopup getTrackingMonthPopup() {
        return trackingMonthPopup;
    }

    public void confirmTrackingNo(ActionEvent actionEvent) {
        this.getTrackingMonthPopup().hide();
            }

    public String confirmTrackingYes() {
        // Add event code here...
        this.getTrackingMonthPopup().hide();
        FacesContext facesContext = FacesContext.getCurrentInstance();
        org.apache.myfaces.trinidad.render.ExtendedRenderKitService service = org.apache.myfaces.trinidad.util.Service.getRenderKitService(facesContext, ExtendedRenderKitService.class);
        StringBuffer jsScript = new StringBuffer();
        String status = "";

         BindingContext bindingctx = BindingContext.getCurrent();
        BindingContainer bindingcnt = bindingctx.getCurrentBindingsEntry();
        DCIteratorBinding headerIter = (DCIteratorBinding) bindingcnt.get("TrxHeadersVO1Iterator");

        Row r =headerIter.getCurrentRow();
        
        OperationBinding callWorkflow = ADFUtils.findOperation("callWorkflow");
        Map paramMap = callWorkflow.getParamsMap();
        paramMap.put("pTxnHeaderId", r.getAttribute("TxnHeaderId"));
        paramMap.put("pUserId", ADFContext.getCurrent().getSessionScope().get("UserId"));
        paramMap.put("pResponsibilityId", ADFContext.getCurrent().getSessionScope().get("RespId"));
        Object obj = callWorkflow.execute();
        
        if (obj == null)
        {
                        showSweetAlertHtmlmessage("Sales Invoice", "<li><span>After submitting workflow no status returned. Kindly check.</span></li>", "error");
                        service.addScript(facesContext, jsScript.toString());
                        return null;
            }
        else if (!"0".equalsIgnoreCase((String)obj))
        {
                        showSweetAlertHtmlmessage("Sales Invoice", "<li><span>"+(String)obj+"</span></li>", "error");
                        service.addScript(facesContext, jsScript.toString());
                        return null;
            }
        
        setValueToEL("#{bindings.Status.inputValue}", "SUBMITTED"); //IN PROCESS
        OperationBinding commit = ADFUtils.findOperation("Commit");
        commit.execute();

        showSweetAlertHtmlmessage("Sales Invoice", "<li><span>Invoice submitted for approval</span></li>", "success");
        service.addScript(facesContext, jsScript.toString());

                return "toSearch";
    }
}
