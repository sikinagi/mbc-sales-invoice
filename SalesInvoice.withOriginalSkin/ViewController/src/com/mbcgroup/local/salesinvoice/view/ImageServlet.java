package com.mbcgroup.local.salesinvoice.view;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.*;
import javax.servlet.http.*;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;

import oracle.jbo.Row;
//import oracle.jbo.domain.BlobDomain;
import java.sql.Blob;
import java.io.OutputStream;

import java.sql.SQLException;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.ADFContext;

import oracle.jbo.domain.BlobDomain;

public class ImageServlet extends HttpServlet {
    private static final String CONTENT_TYPE = "image/gif; charset=UTF-8";

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType(CONTENT_TYPE);
        
        OutputStream os=response.getOutputStream();
        String personId=request.getParameter("personId");
        
        BindingContext bindingContext = BindingContext.getCurrent();
        DCBindingContainer bindings = bindingContext.findBindingContainer("com_mbcgroup_local_salesinvoice_view_pageDefForFilter");
        
        OperationBinding oper = (OperationBinding)bindings.getOperationBinding("getPersonImage");
               oper.getParamsMap().put("PersonId",personId);
                oper.invoke();
                
        Blob img = (Blob)oper.getResult();
                 if (img!=null)
                 {
                     
                BufferedInputStream in=null;
                    try {
                        in = new BufferedInputStream(img.getBinaryStream());
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    int b;
                byte [] buffer =new byte[10240];
                
                    while ((b = in.read(buffer, 0, 10240)) != -1) {
                        os.write(buffer, 0, b);

                    }
                     os.close();
                     in.close();

                }        
                
//        PrintWriter out = response.getWriter();
//        out.println("<html>");
//        out.println("<head><title>ImageServlet</title></head>");
//        out.println("<body>");
//        out.println("<p>The servlet has received a GET. This is the reply.</p>");
//        out.println("</body></html>");
//        out.close();
    }
}
