package com.mbcgroup.local.salesinvoice.view.managedbean;

import com.mbcgroup.local.salesinvoice.util.ADFUtils;

import com.mbcgroup.local.salesinvoice.util.JSFUtils;

import java.io.Serializable;

import java.sql.CallableStatement;

import java.sql.Types;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

//import oracle.jbo.ApplicationModule;

import javax.faces.event.ValueChangeEvent;

import javax.servlet.http.HttpServletRequest;

import oracle.adf.share.ADFContext;

import oracle.binding.OperationBinding;

import oracle.jbo.ApplicationModule;

import oracle.jbo.server.ApplicationModuleImpl;

import org.apache.myfaces.trinidad.util.Service;

import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;

public class SessionBean implements Serializable {
    private String userName;
    private String password;
    private String appLocale="en";

    public SessionBean() {
    }

    public void doLogin(ActionEvent actionEvent) {
        // Add event code here...
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void callJavascript (String javaScript)
    {
                                             FacesContext fctx = FacesContext.getCurrentInstance();
                                             ExtendedRenderKitService erks =
                                             Service.getRenderKitService(fctx, ExtendedRenderKitService.class);
                                             
                                             StringBuilder script = new StringBuilder();
                                             script.append(javaScript);
                                             erks.addScript(fctx, script.toString() );                                      
     }
    
    public String doLogin() {
        // Add event code here...
        //ApplicationModuleImpl am = (ApplicationModuleImpl)ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl");
        
        if (getUserName() == null)
        {
                this.callJavascript("userRequired('Please Enter Username')");
                return null;
            }
        else if (getPassword() == null)
        {
                this.callJavascript("userRequired('Please Enter Password')");
                return null;
            }
       
        try{
            OperationBinding loginOper = ADFUtils.findOperation("doLogin");
            Map pLoginOperMap = loginOper.getParamsMap();
            pLoginOperMap.put("pUserName",getUserName().toUpperCase());
            pLoginOperMap.put("pPassword",getPassword());
            String loginStatus = (String)loginOper.execute();

            if ("1".equalsIgnoreCase(loginStatus))
            {
              //ADFContext.getCurrent().getSessionScope().put("loginUser", this.getUserName().toUpperCase());
              ADFContext.getCurrent().getSessionScope().put("UserName", this.getUserName().toUpperCase());
              
              //String leftMenu = "<a href=\"#\"><i class=\"icon-puzzle icon_style node-ico\" > </i>  <span class=\"sidef node-txt\" >Transactions </span>  <span  class=\"fa fa-angle-left node-cl-arrow\" aria-hidden=\"true\" style=\"font-size:16px;position: absolute;right:15px;top: 9px;\"></span> <span  class=\"fa fa-angle-down node-op-arrow\" aria-hidden=\"true\" style=\"color:White;font-size:16px;display:none;position: absolute;right:15px;top: 12px;\"></span> <span class=\"selected node-edge\" ></span> </a> <ul class=\"sidebar-submenu node-container \" > <li><a href=\"createInvoice.jsf\" class=\"nav\">View Invoice</a></li><li><a href=\"ImportInvoice.jsf\" class=\"nav\">Import</a></li><li><a href=\"ImportInvoice.jsf\" class=\"nav\">Importxxx</a></li></ul></a>";
              //ADFContext.getCurrent().getSessionScope().put("LeftMenu", leftMenu);  
              
                FacesContext fctx = FacesContext.getCurrentInstance();
                HttpServletRequest           request =
                                    (HttpServletRequest)fctx.getExternalContext().getRequest();
                String paramNameValue = request.getParameter("pProfileName");
                String respId = request.getParameter("pRespId");
                
                if (respId == null)
                    respId = "50586";
                ADFContext.getCurrent().getSessionScope().put("RespId", respId);
              
                OperationBinding oper = ADFUtils.findOperation("setContext");
                Map pMap = oper.getParamsMap();
                pMap.put("pUserName",getUserName().toUpperCase());
                pMap.put("pSession",null);
                String retMessage = (String)oper.execute();
                
                if (retMessage != null)
                {
                    if (retMessage.startsWith("Error"))
                    {
                            FacesMessage Message = new FacesMessage(retMessage);   
                              Message.setSeverity(FacesMessage.SEVERITY_ERROR);   
                              FacesContext fc = FacesContext.getCurrentInstance();   
                              fc.addMessage(null, Message); 
                              return null;
                    }
                }

                //Invoke binding operation to get left menu
                OperationBinding menuOper = ADFUtils.findOperation("getMenu");
                Map pMenuOperMap = menuOper.getParamsMap();
                pMenuOperMap.put("pRespId",0); // resp id not being used, just passing for parameters with direct login
                pMenuOperMap.put("pFunctionId",0);  // function id not being used, just passing for parameters with direct login
                menuOper.execute();
                Object err = menuOper.getErrors();
                
              //return "toCreateInvoice";
              return "/page/Home.jsf";
            }
            else
            {
             this.callJavascript("userRequired('Invalid Credentials')");
             return null;
                }
                
        }catch(Exception e)
        {
            System.out.println("Validate 6"+e );  
         }

        return null;
    }

    public void setAppLocale(String appLocale) {
        this.appLocale = appLocale;
    }

    public String getAppLocale() {
        return appLocale;
    }

    public void changeLanguage(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        
        if (valueChangeEvent.getNewValue() != null)
        {
            
            if ("Arabic".equalsIgnoreCase(valueChangeEvent.getNewValue().toString()))
            {
               setAppLocale("ar");
            }
            else
            {
               setAppLocale("en");
            }

             FacesContext fc = FacesContext.getCurrentInstance();
                   String refreshpage = fc.getViewRoot().getViewId();
              ViewHandler ViewH =
              fc.getApplication().getViewHandler();
                   UIViewRoot UIV = ViewH.createView(fc,refreshpage);
                   UIV.setViewId(refreshpage);
                   fc.setViewRoot(UIV);
         }
    }
}
