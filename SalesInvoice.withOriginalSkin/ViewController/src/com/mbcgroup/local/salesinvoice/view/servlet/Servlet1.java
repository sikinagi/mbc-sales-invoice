package com.mbcgroup.local.salesinvoice.view.servlet;


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.*;
import javax.servlet.http.*;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;

import oracle.jbo.Row;
//import oracle.jbo.domain.BlobDomain;
import java.sql.Blob;
import java.io.OutputStream;

import java.sql.SQLException;

import oracle.adf.model.OperationBinding;
import oracle.adf.share.ADFContext;

import oracle.jbo.domain.BlobDomain;

public class Servlet1 extends HttpServlet {
    private static final String CONTENT_TYPE = "application/pdf"; // "image/gif; charset=UTF-8"; //"text/html; charset=UTF-8";

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }


    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
 {
        OutputStream os=response.getOutputStream();
        
        String fileId=request.getParameter("fileId");
        
        //response.setHeader("Content-Disposition","inline; filename=xxxx");
        
       
        
        // if (1==2) {
        
        BindingContext bindingContext = BindingContext.getCurrent();
        DCBindingContainer bindings = bindingContext.findBindingContainer("com_mbcgroup_local_salesinvoice_view_pageDefForFilter");
        
        OperationBinding oper = (OperationBinding)bindings.getOperationBinding("getAttach");
       oper.getParamsMap().put("AttachId",fileId);
        oper.invoke();
        
        // BlobDomain img = (BlobDomain)oper.getResult();
        Blob img = (Blob)oper.getResult();
         if (img!=null)
         {
             
        BufferedInputStream in=null;
            try {
                in = new BufferedInputStream(img.getBinaryStream());
            } catch (SQLException e) {
                e.printStackTrace();
            }
            int b;
        byte [] buffer =new byte[10240];
        
            while ((b = in.read(buffer, 0, 10240)) != -1) {
                os.write(buffer, 0, b);

            }
             os.close();
             in.close();

        }
        response.setHeader("Content-Disposition","attachment; filename=\"MyFile.pdf\"");
        response.setContentType("application/pdf; name=\"MyFile.pdf\"");                                                                             
    }
    
}
